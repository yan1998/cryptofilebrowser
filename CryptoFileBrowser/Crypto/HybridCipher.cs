﻿using System;
using System.Security.Cryptography;

namespace CryptoFileBrowser.Crypto
{
    class HybridCipher : Cipher
    {
        private Cipher aes = new AES();
        private Cipher trippleDes = new TripleDES();

        public override byte[] Decrypt(byte[] bytes, byte[] key)
        {
            byte[] tempCode = trippleDes.Decrypt(bytes, key);
            return aes.Decrypt(tempCode, key); 
        }

        public override byte[] Encrypt(byte[] bytes, byte[] key)
        {
            byte[] tempCode=aes.Encrypt(bytes, key);
            return trippleDes.Encrypt(tempCode, key);
        }
    }
}
