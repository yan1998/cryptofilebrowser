﻿using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoFileBrowser.Crypto
{
    class TripleDES : Cipher
    {
        private TripleDESCryptoServiceProvider trippleDES;

        public TripleDES()
        {
            trippleDES = new TripleDESCryptoServiceProvider();
            trippleDES.Mode = CipherMode.ECB;
            trippleDES.Padding = PaddingMode.PKCS7;
        }
        
        public override byte[] Decrypt(byte[] bytes, byte[] key)
        {
            trippleDES.Key = key;
            ICryptoTransform trans = trippleDES.CreateDecryptor();
            byte[] result = trans.TransformFinalBlock(bytes, 0, bytes.Length);
            return result;
        }

        public override byte[] Encrypt(byte[] bytes, byte[] key)
        {
            //TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider();
            trippleDES.Key = key;
            ICryptoTransform trans= trippleDES.CreateEncryptor();
            byte[] result = trans.TransformFinalBlock(bytes, 0, bytes.Length);
            return result;
        }
    }
}
