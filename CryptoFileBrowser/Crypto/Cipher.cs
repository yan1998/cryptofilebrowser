﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoFileBrowser.Crypto
{
    public abstract class Cipher
    {
        public abstract byte[] Decrypt(byte[] bytes, byte[] key);
        public string Decrypt(string str, string key)
        {
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(str),
                keyBytes = UTF8Encoding.UTF8.GetBytes(key);
            byte[] resultBytes = Decrypt(bytes, keyBytes);
            return Convert.ToBase64String(resultBytes);
        }

        public abstract byte[] Encrypt(byte[] bytes, byte[] key);
        public string Encrypt(string str, string key)
        {
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(str),
                            keyBytes = UTF8Encoding.UTF8.GetBytes(key);
            byte[] resultBytes = Encrypt(bytes, keyBytes);
            return Convert.ToBase64String(resultBytes);
        }
    }
}
