﻿using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoFileBrowser.Crypto
{
    class AES : Cipher
    {
        private Aes cipher;
        
        public AES()
        {
            cipher = Aes.Create();
            cipher.Padding = PaddingMode.PKCS7;
            cipher.Mode = CipherMode.ECB;
        }

        public override byte[] Decrypt(byte[] bytes, byte[] key)
        {
            cipher.Key = key;
            ICryptoTransform trans = cipher.CreateDecryptor(key, cipher.IV);
            return trans.TransformFinalBlock(bytes, 0, bytes.Length);
        }

        public override byte[] Encrypt(byte[] bytes, byte[] key)
        {
            cipher.Key = key;
            ICryptoTransform trans= cipher.CreateEncryptor(key, cipher.IV);
            return trans.TransformFinalBlock(bytes,0,bytes.Length);
        }
    }
}
