﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace CryptoFileBrowser.Crypto
{
    public class MD5Hash
    {
        public string GetHashCode(string data)
        {
            byte[] personalKeyBytes;
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] byteStr = UTF8Encoding.UTF8.GetBytes(data);
                personalKeyBytes = md5.ComputeHash(byteStr);
            }
            return Convert.ToBase64String(personalKeyBytes, 0, personalKeyBytes.Length);
        }
    }
}
