﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Text;
using CryptoFileBrowser.Models;
using System.Threading.Tasks;

namespace CryptoFileBrowser.Forms
{
    public partial class Authorization : MetroFramework.Forms.MetroForm
    {
        private string personalKey=String.Empty;
        private DataBaseContext context;

        public Authorization()
        {
            InitializeComponent();
            context = new DataBaseContext();
        }

        //Открытие формы регистрации
        private void metroLinkRegistry_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Registration registration = new Registration(this);
            registration.Show();
        }

        //Открытие файла с ключом
        private async void metroButtonBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                if (openPersonalKeyDialog.ShowDialog() == DialogResult.OK)
                {
                    using (Stream stream = openPersonalKeyDialog.OpenFile())
                    {
                        byte[] buffer = new byte[stream.Length];
                        await stream.ReadAsync(buffer, 0, (int)stream.Length);
                        personalKey = UTF8Encoding.UTF8.GetString(buffer);
                        personalKey=personalKey.Remove(personalKey.Length - 2, 2);
                    }
                    metroLabelFile.Text = "Ключ был успешно считан!";
                    metroButtonEnter.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Авторизация
        private void metroButtonEnter_Click(object sender, EventArgs e)
        {
            string login = metroTextBoxLogin.Text,
                password = metroTextBoxPassword.Text;
            if (login == "" || password == "")
            {
                MetroFramework.MetroMessageBox.Show(this, "Заполните все поля!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var users = context.Users.Where(u => u.Login == login && u.Password == password && u.PersonalKey == personalKey).ToList();
            if (users.Count != 0)
            {
                this.Visible = false;
                Cabinet cabinet = new Cabinet(users.Single());
                cabinet.Show();
            }
            else
                MetroFramework.MetroMessageBox.Show(this,"Проверьте правильность ввода пароля!","Ошибка",MessageBoxButtons.OK,MessageBoxIcon.Error);
        }

        private void metroLinkPasswordRestore_Click(object sender, EventArgs e)
        {
            PasswordRestore restore = new PasswordRestore();
            restore.ShowDialog();
        }
    }
}
