﻿namespace CryptoFileBrowser.Forms
{
    partial class PasswordRestore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswordRestore));
            this.metroTextBoxRestoreQuestion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxRestoreAnswer = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabelFile = new MetroFramework.Controls.MetroLabel();
            this.metroButtonBrowse = new MetroFramework.Controls.MetroButton();
            this.openFileDialogPersonalKey = new System.Windows.Forms.OpenFileDialog();
            this.metroButtonOk = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonFind = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroTextBoxRestoreQuestion
            // 
            // 
            // 
            // 
            this.metroTextBoxRestoreQuestion.CustomButton.Image = null;
            this.metroTextBoxRestoreQuestion.CustomButton.Location = new System.Drawing.Point(268, 2);
            this.metroTextBoxRestoreQuestion.CustomButton.Name = "";
            this.metroTextBoxRestoreQuestion.CustomButton.Size = new System.Drawing.Size(63, 63);
            this.metroTextBoxRestoreQuestion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxRestoreQuestion.CustomButton.TabIndex = 1;
            this.metroTextBoxRestoreQuestion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxRestoreQuestion.CustomButton.UseSelectable = true;
            this.metroTextBoxRestoreQuestion.CustomButton.Visible = false;
            this.metroTextBoxRestoreQuestion.Lines = new string[0];
            this.metroTextBoxRestoreQuestion.Location = new System.Drawing.Point(23, 135);
            this.metroTextBoxRestoreQuestion.MaxLength = 32767;
            this.metroTextBoxRestoreQuestion.Multiline = true;
            this.metroTextBoxRestoreQuestion.Name = "metroTextBoxRestoreQuestion";
            this.metroTextBoxRestoreQuestion.PasswordChar = '\0';
            this.metroTextBoxRestoreQuestion.ReadOnly = true;
            this.metroTextBoxRestoreQuestion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxRestoreQuestion.SelectedText = "";
            this.metroTextBoxRestoreQuestion.SelectionLength = 0;
            this.metroTextBoxRestoreQuestion.SelectionStart = 0;
            this.metroTextBoxRestoreQuestion.ShortcutsEnabled = true;
            this.metroTextBoxRestoreQuestion.Size = new System.Drawing.Size(334, 68);
            this.metroTextBoxRestoreQuestion.TabIndex = 2;
            this.metroTextBoxRestoreQuestion.UseSelectable = true;
            this.metroTextBoxRestoreQuestion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxRestoreQuestion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(17, 107);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(58, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Вопрос:";
            // 
            // metroTextBoxRestoreAnswer
            // 
            // 
            // 
            // 
            this.metroTextBoxRestoreAnswer.CustomButton.Image = null;
            this.metroTextBoxRestoreAnswer.CustomButton.Location = new System.Drawing.Point(224, 1);
            this.metroTextBoxRestoreAnswer.CustomButton.Name = "";
            this.metroTextBoxRestoreAnswer.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxRestoreAnswer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxRestoreAnswer.CustomButton.TabIndex = 1;
            this.metroTextBoxRestoreAnswer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxRestoreAnswer.CustomButton.UseSelectable = true;
            this.metroTextBoxRestoreAnswer.CustomButton.Visible = false;
            this.metroTextBoxRestoreAnswer.Lines = new string[0];
            this.metroTextBoxRestoreAnswer.Location = new System.Drawing.Point(111, 225);
            this.metroTextBoxRestoreAnswer.MaxLength = 32767;
            this.metroTextBoxRestoreAnswer.Name = "metroTextBoxRestoreAnswer";
            this.metroTextBoxRestoreAnswer.PasswordChar = '\0';
            this.metroTextBoxRestoreAnswer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxRestoreAnswer.SelectedText = "";
            this.metroTextBoxRestoreAnswer.SelectionLength = 0;
            this.metroTextBoxRestoreAnswer.SelectionStart = 0;
            this.metroTextBoxRestoreAnswer.ShortcutsEnabled = true;
            this.metroTextBoxRestoreAnswer.Size = new System.Drawing.Size(246, 23);
            this.metroTextBoxRestoreAnswer.TabIndex = 3;
            this.metroTextBoxRestoreAnswer.UseSelectable = true;
            this.metroTextBoxRestoreAnswer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxRestoreAnswer.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(29, 226);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(47, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Ответ:";
            // 
            // metroLabelFile
            // 
            this.metroLabelFile.AutoSize = true;
            this.metroLabelFile.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabelFile.Location = new System.Drawing.Point(170, 276);
            this.metroLabelFile.Name = "metroLabelFile";
            this.metroLabelFile.Size = new System.Drawing.Size(141, 15);
            this.metroLabelFile.TabIndex = 4;
            this.metroLabelFile.Text = "Выберите файл с ключом";
            // 
            // metroButtonBrowse
            // 
            this.metroButtonBrowse.Location = new System.Drawing.Point(97, 272);
            this.metroButtonBrowse.Name = "metroButtonBrowse";
            this.metroButtonBrowse.Size = new System.Drawing.Size(67, 23);
            this.metroButtonBrowse.TabIndex = 4;
            this.metroButtonBrowse.Text = "Обзор";
            this.metroButtonBrowse.UseSelectable = true;
            this.metroButtonBrowse.Click += new System.EventHandler(this.metroButtonBrowse_Click);
            // 
            // openFileDialogPersonalKey
            // 
            this.openFileDialogPersonalKey.FileName = "openFileDialog1";
            // 
            // metroButtonOk
            // 
            this.metroButtonOk.Enabled = false;
            this.metroButtonOk.Location = new System.Drawing.Point(28, 318);
            this.metroButtonOk.Name = "metroButtonOk";
            this.metroButtonOk.Size = new System.Drawing.Size(154, 47);
            this.metroButtonOk.TabIndex = 5;
            this.metroButtonOk.Text = "Напомнить пароль";
            this.metroButtonOk.UseSelectable = true;
            this.metroButtonOk.Click += new System.EventHandler(this.metroButtonOk_Click);
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(209, 318);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(148, 47);
            this.metroButtonCancel.TabIndex = 6;
            this.metroButtonCancel.Text = "Отмена";
            this.metroButtonCancel.UseSelectable = true;
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(17, 70);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(50, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "E-mail:";
            // 
            // metroTextBoxEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxEmail.CustomButton.Image = null;
            this.metroTextBoxEmail.CustomButton.Location = new System.Drawing.Point(175, 1);
            this.metroTextBoxEmail.CustomButton.Name = "";
            this.metroTextBoxEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEmail.CustomButton.TabIndex = 1;
            this.metroTextBoxEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxEmail.CustomButton.Visible = false;
            this.metroTextBoxEmail.Lines = new string[0];
            this.metroTextBoxEmail.Location = new System.Drawing.Point(81, 69);
            this.metroTextBoxEmail.MaxLength = 32767;
            this.metroTextBoxEmail.Name = "metroTextBoxEmail";
            this.metroTextBoxEmail.PasswordChar = '\0';
            this.metroTextBoxEmail.PromptText = "example@gmail.com";
            this.metroTextBoxEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEmail.SelectedText = "";
            this.metroTextBoxEmail.SelectionLength = 0;
            this.metroTextBoxEmail.SelectionStart = 0;
            this.metroTextBoxEmail.ShortcutsEnabled = true;
            this.metroTextBoxEmail.Size = new System.Drawing.Size(197, 23);
            this.metroTextBoxEmail.TabIndex = 0;
            this.metroTextBoxEmail.UseSelectable = true;
            this.metroTextBoxEmail.WaterMark = "example@gmail.com";
            this.metroTextBoxEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonFind
            // 
            this.metroButtonFind.Location = new System.Drawing.Point(285, 69);
            this.metroButtonFind.Name = "metroButtonFind";
            this.metroButtonFind.Size = new System.Drawing.Size(75, 23);
            this.metroButtonFind.TabIndex = 1;
            this.metroButtonFind.Text = "Поиск";
            this.metroButtonFind.UseSelectable = true;
            this.metroButtonFind.Click += new System.EventHandler(this.metroButtonFind_Click);
            // 
            // PasswordRestore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 390);
            this.Controls.Add(this.metroButtonFind);
            this.Controls.Add(this.metroTextBoxEmail);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonOk);
            this.Controls.Add(this.metroLabelFile);
            this.Controls.Add(this.metroButtonBrowse);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroTextBoxRestoreAnswer);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroTextBoxRestoreQuestion);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PasswordRestore";
            this.Resizable = false;
            this.Text = "Восстановление пароля";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox metroTextBoxRestoreQuestion;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox metroTextBoxRestoreAnswer;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabelFile;
        private MetroFramework.Controls.MetroButton metroButtonBrowse;
        private System.Windows.Forms.OpenFileDialog openFileDialogPersonalKey;
        private MetroFramework.Controls.MetroButton metroButtonOk;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEmail;
        private MetroFramework.Controls.MetroButton metroButtonFind;
    }
}