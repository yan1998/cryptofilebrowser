﻿namespace CryptoFileBrowser.Forms
{
    partial class Cabinet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cabinet));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPageProfile = new MetroFramework.Controls.MetroTabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.metroPanelUserInfo = new MetroFramework.Controls.MetroPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroButtonChangeInfo = new MetroFramework.Controls.MetroButton();
            this.metroLabelName = new MetroFramework.Controls.MetroLabel();
            this.metroButtonChangePersonalKey = new MetroFramework.Controls.MetroButton();
            this.metroLabelSurname = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEmail = new MetroFramework.Controls.MetroLabel();
            this.metroPanelInterface = new MetroFramework.Controls.MetroPanel();
            this.metroButtonReset = new MetroFramework.Controls.MetroButton();
            this.metroButtonSaveStyle = new MetroFramework.Controls.MetroButton();
            this.metroComboBoxStyle = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroToggleTheme = new MetroFramework.Controls.MetroToggle();
            this.metroTabPageAddFiles = new MetroFramework.Controls.MetroTabPage();
            this.metroProgressSpinnerAddFile = new MetroFramework.Controls.MetroProgressSpinner();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroPanelFileTypes = new MetroFramework.Controls.MetroPanel();
            this.metroRadioButtonTypeMedia = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButtonTypeGraphic = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButtonTypeText = new MetroFramework.Controls.MetroRadioButton();
            this.metroButtonAddFile = new MetroFramework.Controls.MetroButton();
            this.metroLabelAddFileName = new MetroFramework.Controls.MetroLabel();
            this.metroButtonBrowse = new MetroFramework.Controls.MetroButton();
            this.metroLabelCipher = new MetroFramework.Controls.MetroLabel();
            this.metroToggleCipher = new MetroFramework.Controls.MetroToggle();
            this.metroTabPageBrowseFiles = new MetroFramework.Controls.MetroTabPage();
            this.metroPanelTypesFileView = new MetroFramework.Controls.MetroPanel();
            this.metroRadioButton1 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton2 = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton3 = new MetroFramework.Controls.MetroRadioButton();
            this.flowLayoutPanelFiles = new System.Windows.Forms.FlowLayoutPanel();
            this.metroTabPageExport = new MetroFramework.Controls.MetroTabPage();
            this.metroPanelExportFiles = new System.Windows.Forms.Panel();
            this.metroLabelExportSize = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroProgressSpinnerExport = new MetroFramework.Controls.MetroProgressSpinner();
            this.metroButtonExportInEmail = new MetroFramework.Controls.MetroButton();
            this.metroButtonExportInFolder = new MetroFramework.Controls.MetroButton();
            this.metroStyleManager = new MetroFramework.Components.MetroStyleManager(this.components);
            this.addFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialogExport = new System.Windows.Forms.SaveFileDialog();
            this.metroTabPageListFiles = new MetroFramework.Controls.MetroTabPage();
            this.metroGridFiles = new MetroFramework.Controls.MetroGrid();
            this.metroComboBoxSort = new MetroFramework.Controls.MetroComboBox();
            this.fileBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroTabControl1.SuspendLayout();
            this.metroTabPageProfile.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.metroPanelUserInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.metroPanelInterface.SuspendLayout();
            this.metroTabPageAddFiles.SuspendLayout();
            this.metroPanelFileTypes.SuspendLayout();
            this.metroTabPageBrowseFiles.SuspendLayout();
            this.metroPanelTypesFileView.SuspendLayout();
            this.metroTabPageExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).BeginInit();
            this.metroTabPageListFiles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            resources.ApplyResources(this.metroTabControl1, "metroTabControl1");
            this.metroTabControl1.Controls.Add(this.metroTabPageProfile);
            this.metroTabControl1.Controls.Add(this.metroTabPageAddFiles);
            this.metroTabControl1.Controls.Add(this.metroTabPageBrowseFiles);
            this.metroTabControl1.Controls.Add(this.metroTabPageListFiles);
            this.metroTabControl1.Controls.Add(this.metroTabPageExport);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            this.metroTabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.metroTabControl1_Selected);
            // 
            // metroTabPageProfile
            // 
            this.metroTabPageProfile.Controls.Add(this.flowLayoutPanel1);
            this.metroTabPageProfile.HorizontalScrollbarBarColor = true;
            this.metroTabPageProfile.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageProfile.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroTabPageProfile, "metroTabPageProfile");
            this.metroTabPageProfile.Name = "metroTabPageProfile";
            this.metroTabPageProfile.VerticalScrollbarBarColor = true;
            this.metroTabPageProfile.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageProfile.VerticalScrollbarSize = 10;
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.metroPanelUserInfo);
            this.flowLayoutPanel1.Controls.Add(this.metroPanelInterface);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // metroPanelUserInfo
            // 
            this.metroPanelUserInfo.Controls.Add(this.pictureBox1);
            this.metroPanelUserInfo.Controls.Add(this.metroButtonChangeInfo);
            this.metroPanelUserInfo.Controls.Add(this.metroLabelName);
            this.metroPanelUserInfo.Controls.Add(this.metroButtonChangePersonalKey);
            this.metroPanelUserInfo.Controls.Add(this.metroLabelSurname);
            this.metroPanelUserInfo.Controls.Add(this.metroLabelEmail);
            this.metroPanelUserInfo.HorizontalScrollbarBarColor = true;
            this.metroPanelUserInfo.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanelUserInfo.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroPanelUserInfo, "metroPanelUserInfo");
            this.metroPanelUserInfo.Name = "metroPanelUserInfo";
            this.metroPanelUserInfo.VerticalScrollbarBarColor = true;
            this.metroPanelUserInfo.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanelUserInfo.VerticalScrollbarSize = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Silver;
            this.pictureBox1.Image = global::CryptoFileBrowser.Properties.Resources.user;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // metroButtonChangeInfo
            // 
            resources.ApplyResources(this.metroButtonChangeInfo, "metroButtonChangeInfo");
            this.metroButtonChangeInfo.Name = "metroButtonChangeInfo";
            this.metroButtonChangeInfo.UseSelectable = true;
            this.metroButtonChangeInfo.Click += new System.EventHandler(this.metroButtonChangeInfo_Click);
            // 
            // metroLabelName
            // 
            resources.ApplyResources(this.metroLabelName, "metroLabelName");
            this.metroLabelName.Name = "metroLabelName";
            // 
            // metroButtonChangePersonalKey
            // 
            resources.ApplyResources(this.metroButtonChangePersonalKey, "metroButtonChangePersonalKey");
            this.metroButtonChangePersonalKey.Name = "metroButtonChangePersonalKey";
            this.metroButtonChangePersonalKey.UseSelectable = true;
            this.metroButtonChangePersonalKey.Click += new System.EventHandler(this.metroButtonChangePersonalKey_Click);
            // 
            // metroLabelSurname
            // 
            resources.ApplyResources(this.metroLabelSurname, "metroLabelSurname");
            this.metroLabelSurname.Name = "metroLabelSurname";
            // 
            // metroLabelEmail
            // 
            resources.ApplyResources(this.metroLabelEmail, "metroLabelEmail");
            this.metroLabelEmail.Name = "metroLabelEmail";
            // 
            // metroPanelInterface
            // 
            this.metroPanelInterface.Controls.Add(this.metroButtonReset);
            this.metroPanelInterface.Controls.Add(this.metroButtonSaveStyle);
            this.metroPanelInterface.Controls.Add(this.metroComboBoxStyle);
            this.metroPanelInterface.Controls.Add(this.metroLabel3);
            this.metroPanelInterface.Controls.Add(this.metroLabel2);
            this.metroPanelInterface.Controls.Add(this.metroLabel1);
            this.metroPanelInterface.Controls.Add(this.metroToggleTheme);
            this.metroPanelInterface.HorizontalScrollbarBarColor = true;
            this.metroPanelInterface.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanelInterface.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroPanelInterface, "metroPanelInterface");
            this.metroPanelInterface.Name = "metroPanelInterface";
            this.metroPanelInterface.VerticalScrollbarBarColor = true;
            this.metroPanelInterface.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanelInterface.VerticalScrollbarSize = 10;
            // 
            // metroButtonReset
            // 
            resources.ApplyResources(this.metroButtonReset, "metroButtonReset");
            this.metroButtonReset.Name = "metroButtonReset";
            this.metroButtonReset.UseSelectable = true;
            this.metroButtonReset.Click += new System.EventHandler(this.metroButtonReset_Click);
            // 
            // metroButtonSaveStyle
            // 
            resources.ApplyResources(this.metroButtonSaveStyle, "metroButtonSaveStyle");
            this.metroButtonSaveStyle.Name = "metroButtonSaveStyle";
            this.metroButtonSaveStyle.UseSelectable = true;
            this.metroButtonSaveStyle.Click += new System.EventHandler(this.metroButtonSaveStyle_Click);
            // 
            // metroComboBoxStyle
            // 
            this.metroComboBoxStyle.FormattingEnabled = true;
            resources.ApplyResources(this.metroComboBoxStyle, "metroComboBoxStyle");
            this.metroComboBoxStyle.Items.AddRange(new object[] {
            resources.GetString("metroComboBoxStyle.Items"),
            resources.GetString("metroComboBoxStyle.Items1"),
            resources.GetString("metroComboBoxStyle.Items2"),
            resources.GetString("metroComboBoxStyle.Items3"),
            resources.GetString("metroComboBoxStyle.Items4"),
            resources.GetString("metroComboBoxStyle.Items5"),
            resources.GetString("metroComboBoxStyle.Items6"),
            resources.GetString("metroComboBoxStyle.Items7"),
            resources.GetString("metroComboBoxStyle.Items8"),
            resources.GetString("metroComboBoxStyle.Items9"),
            resources.GetString("metroComboBoxStyle.Items10"),
            resources.GetString("metroComboBoxStyle.Items11"),
            resources.GetString("metroComboBoxStyle.Items12"),
            resources.GetString("metroComboBoxStyle.Items13"),
            resources.GetString("metroComboBoxStyle.Items14")});
            this.metroComboBoxStyle.Name = "metroComboBoxStyle";
            this.metroComboBoxStyle.UseSelectable = true;
            this.metroComboBoxStyle.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxStyle_SelectedIndexChanged);
            // 
            // metroLabel3
            // 
            resources.ApplyResources(this.metroLabel3, "metroLabel3");
            this.metroLabel3.Name = "metroLabel3";
            // 
            // metroLabel2
            // 
            resources.ApplyResources(this.metroLabel2, "metroLabel2");
            this.metroLabel2.Name = "metroLabel2";
            // 
            // metroLabel1
            // 
            resources.ApplyResources(this.metroLabel1, "metroLabel1");
            this.metroLabel1.Name = "metroLabel1";
            // 
            // metroToggleTheme
            // 
            resources.ApplyResources(this.metroToggleTheme, "metroToggleTheme");
            this.metroToggleTheme.Name = "metroToggleTheme";
            this.metroToggleTheme.UseSelectable = true;
            this.metroToggleTheme.CheckedChanged += new System.EventHandler(this.metroToggleTheme_CheckedChanged);
            // 
            // metroTabPageAddFiles
            // 
            this.metroTabPageAddFiles.Controls.Add(this.metroProgressSpinnerAddFile);
            this.metroTabPageAddFiles.Controls.Add(this.metroLabel4);
            this.metroTabPageAddFiles.Controls.Add(this.metroPanelFileTypes);
            this.metroTabPageAddFiles.Controls.Add(this.metroButtonAddFile);
            this.metroTabPageAddFiles.Controls.Add(this.metroLabelAddFileName);
            this.metroTabPageAddFiles.Controls.Add(this.metroButtonBrowse);
            this.metroTabPageAddFiles.Controls.Add(this.metroLabelCipher);
            this.metroTabPageAddFiles.Controls.Add(this.metroToggleCipher);
            this.metroTabPageAddFiles.HorizontalScrollbarBarColor = true;
            this.metroTabPageAddFiles.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageAddFiles.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroTabPageAddFiles, "metroTabPageAddFiles");
            this.metroTabPageAddFiles.Name = "metroTabPageAddFiles";
            this.metroTabPageAddFiles.VerticalScrollbarBarColor = true;
            this.metroTabPageAddFiles.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageAddFiles.VerticalScrollbarSize = 10;
            // 
            // metroProgressSpinnerAddFile
            // 
            resources.ApplyResources(this.metroProgressSpinnerAddFile, "metroProgressSpinnerAddFile");
            this.metroProgressSpinnerAddFile.Maximum = 100;
            this.metroProgressSpinnerAddFile.Name = "metroProgressSpinnerAddFile";
            this.metroProgressSpinnerAddFile.UseSelectable = true;
            this.metroProgressSpinnerAddFile.Value = 20;
            // 
            // metroLabel4
            // 
            resources.ApplyResources(this.metroLabel4, "metroLabel4");
            this.metroLabel4.Name = "metroLabel4";
            // 
            // metroPanelFileTypes
            // 
            resources.ApplyResources(this.metroPanelFileTypes, "metroPanelFileTypes");
            this.metroPanelFileTypes.Controls.Add(this.metroRadioButtonTypeMedia);
            this.metroPanelFileTypes.Controls.Add(this.metroRadioButtonTypeGraphic);
            this.metroPanelFileTypes.Controls.Add(this.metroRadioButtonTypeText);
            this.metroPanelFileTypes.HorizontalScrollbarBarColor = true;
            this.metroPanelFileTypes.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanelFileTypes.HorizontalScrollbarSize = 10;
            this.metroPanelFileTypes.Name = "metroPanelFileTypes";
            this.metroPanelFileTypes.VerticalScrollbarBarColor = true;
            this.metroPanelFileTypes.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanelFileTypes.VerticalScrollbarSize = 10;
            // 
            // metroRadioButtonTypeMedia
            // 
            resources.ApplyResources(this.metroRadioButtonTypeMedia, "metroRadioButtonTypeMedia");
            this.metroRadioButtonTypeMedia.Name = "metroRadioButtonTypeMedia";
            this.metroRadioButtonTypeMedia.UseSelectable = true;
            // 
            // metroRadioButtonTypeGraphic
            // 
            resources.ApplyResources(this.metroRadioButtonTypeGraphic, "metroRadioButtonTypeGraphic");
            this.metroRadioButtonTypeGraphic.Name = "metroRadioButtonTypeGraphic";
            this.metroRadioButtonTypeGraphic.UseSelectable = true;
            // 
            // metroRadioButtonTypeText
            // 
            resources.ApplyResources(this.metroRadioButtonTypeText, "metroRadioButtonTypeText");
            this.metroRadioButtonTypeText.Checked = true;
            this.metroRadioButtonTypeText.Name = "metroRadioButtonTypeText";
            this.metroRadioButtonTypeText.TabStop = true;
            this.metroRadioButtonTypeText.UseSelectable = true;
            // 
            // metroButtonAddFile
            // 
            resources.ApplyResources(this.metroButtonAddFile, "metroButtonAddFile");
            this.metroButtonAddFile.Name = "metroButtonAddFile";
            this.metroButtonAddFile.UseSelectable = true;
            this.metroButtonAddFile.Click += new System.EventHandler(this.metroButtonAddFile_Click);
            // 
            // metroLabelAddFileName
            // 
            resources.ApplyResources(this.metroLabelAddFileName, "metroLabelAddFileName");
            this.metroLabelAddFileName.Name = "metroLabelAddFileName";
            // 
            // metroButtonBrowse
            // 
            resources.ApplyResources(this.metroButtonBrowse, "metroButtonBrowse");
            this.metroButtonBrowse.Name = "metroButtonBrowse";
            this.metroButtonBrowse.UseSelectable = true;
            this.metroButtonBrowse.Click += new System.EventHandler(this.metroButtonBrowse_Click);
            // 
            // metroLabelCipher
            // 
            resources.ApplyResources(this.metroLabelCipher, "metroLabelCipher");
            this.metroLabelCipher.Name = "metroLabelCipher";
            // 
            // metroToggleCipher
            // 
            resources.ApplyResources(this.metroToggleCipher, "metroToggleCipher");
            this.metroToggleCipher.Name = "metroToggleCipher";
            this.metroToggleCipher.UseSelectable = true;
            // 
            // metroTabPageBrowseFiles
            // 
            this.metroTabPageBrowseFiles.Controls.Add(this.flowLayoutPanelFiles);
            this.metroTabPageBrowseFiles.HorizontalScrollbarBarColor = true;
            this.metroTabPageBrowseFiles.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageBrowseFiles.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroTabPageBrowseFiles, "metroTabPageBrowseFiles");
            this.metroTabPageBrowseFiles.Name = "metroTabPageBrowseFiles";
            this.metroTabPageBrowseFiles.VerticalScrollbarBarColor = true;
            this.metroTabPageBrowseFiles.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageBrowseFiles.VerticalScrollbarSize = 10;
            // 
            // metroPanelTypesFileView
            // 
            this.metroPanelTypesFileView.Controls.Add(this.metroComboBoxSort);
            this.metroPanelTypesFileView.Controls.Add(this.metroRadioButton1);
            this.metroPanelTypesFileView.Controls.Add(this.metroRadioButton2);
            this.metroPanelTypesFileView.Controls.Add(this.metroRadioButton3);
            this.metroPanelTypesFileView.HorizontalScrollbarBarColor = true;
            this.metroPanelTypesFileView.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanelTypesFileView.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroPanelTypesFileView, "metroPanelTypesFileView");
            this.metroPanelTypesFileView.Name = "metroPanelTypesFileView";
            this.metroPanelTypesFileView.VerticalScrollbarBarColor = true;
            this.metroPanelTypesFileView.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanelTypesFileView.VerticalScrollbarSize = 10;
            // 
            // metroRadioButton1
            // 
            resources.ApplyResources(this.metroRadioButton1, "metroRadioButton1");
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Tag = "Медиа";
            this.metroRadioButton1.UseSelectable = true;
            this.metroRadioButton1.CheckedChanged += new System.EventHandler(this.metroRadioButton3_CheckedChanged);
            // 
            // metroRadioButton2
            // 
            resources.ApplyResources(this.metroRadioButton2, "metroRadioButton2");
            this.metroRadioButton2.Name = "metroRadioButton2";
            this.metroRadioButton2.Tag = "Графический";
            this.metroRadioButton2.UseSelectable = true;
            this.metroRadioButton2.CheckedChanged += new System.EventHandler(this.metroRadioButton3_CheckedChanged);
            // 
            // metroRadioButton3
            // 
            resources.ApplyResources(this.metroRadioButton3, "metroRadioButton3");
            this.metroRadioButton3.Checked = true;
            this.metroRadioButton3.Name = "metroRadioButton3";
            this.metroRadioButton3.TabStop = true;
            this.metroRadioButton3.Tag = "Текстовый";
            this.metroRadioButton3.UseSelectable = true;
            this.metroRadioButton3.CheckedChanged += new System.EventHandler(this.metroRadioButton3_CheckedChanged);
            // 
            // flowLayoutPanelFiles
            // 
            resources.ApplyResources(this.flowLayoutPanelFiles, "flowLayoutPanelFiles");
            this.flowLayoutPanelFiles.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanelFiles.Name = "flowLayoutPanelFiles";
            // 
            // metroTabPageExport
            // 
            this.metroTabPageExport.Controls.Add(this.metroPanelExportFiles);
            this.metroTabPageExport.Controls.Add(this.metroLabelExportSize);
            this.metroTabPageExport.Controls.Add(this.metroLabel5);
            this.metroTabPageExport.Controls.Add(this.metroProgressSpinnerExport);
            this.metroTabPageExport.Controls.Add(this.metroButtonExportInEmail);
            this.metroTabPageExport.Controls.Add(this.metroButtonExportInFolder);
            this.metroTabPageExport.HorizontalScrollbarBarColor = true;
            this.metroTabPageExport.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageExport.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroTabPageExport, "metroTabPageExport");
            this.metroTabPageExport.Name = "metroTabPageExport";
            this.metroTabPageExport.VerticalScrollbarBarColor = true;
            this.metroTabPageExport.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageExport.VerticalScrollbarSize = 10;
            // 
            // metroPanelExportFiles
            // 
            resources.ApplyResources(this.metroPanelExportFiles, "metroPanelExportFiles");
            this.metroPanelExportFiles.BackColor = System.Drawing.Color.Transparent;
            this.metroPanelExportFiles.Name = "metroPanelExportFiles";
            // 
            // metroLabelExportSize
            // 
            resources.ApplyResources(this.metroLabelExportSize, "metroLabelExportSize");
            this.metroLabelExportSize.Name = "metroLabelExportSize";
            // 
            // metroLabel5
            // 
            resources.ApplyResources(this.metroLabel5, "metroLabel5");
            this.metroLabel5.Name = "metroLabel5";
            // 
            // metroProgressSpinnerExport
            // 
            resources.ApplyResources(this.metroProgressSpinnerExport, "metroProgressSpinnerExport");
            this.metroProgressSpinnerExport.Maximum = 100;
            this.metroProgressSpinnerExport.Name = "metroProgressSpinnerExport";
            this.metroProgressSpinnerExport.UseSelectable = true;
            this.metroProgressSpinnerExport.Value = 30;
            // 
            // metroButtonExportInEmail
            // 
            resources.ApplyResources(this.metroButtonExportInEmail, "metroButtonExportInEmail");
            this.metroButtonExportInEmail.Name = "metroButtonExportInEmail";
            this.metroButtonExportInEmail.UseSelectable = true;
            this.metroButtonExportInEmail.Click += new System.EventHandler(this.metroButtonExportInEmail_Click);
            // 
            // metroButtonExportInFolder
            // 
            resources.ApplyResources(this.metroButtonExportInFolder, "metroButtonExportInFolder");
            this.metroButtonExportInFolder.Name = "metroButtonExportInFolder";
            this.metroButtonExportInFolder.UseSelectable = true;
            this.metroButtonExportInFolder.Click += new System.EventHandler(this.metroButtonExportInFolder_Click);
            // 
            // metroStyleManager
            // 
            this.metroStyleManager.Owner = this;
            // 
            // addFileDialog
            // 
            resources.ApplyResources(this.addFileDialog, "addFileDialog");
            // 
            // folderBrowserDialogExport
            // 
            this.folderBrowserDialogExport.DefaultExt = "zip";
            this.folderBrowserDialogExport.FileName = "exportFiles";
            resources.ApplyResources(this.folderBrowserDialogExport, "folderBrowserDialogExport");
            // 
            // metroTabPageListFiles
            // 
            this.metroTabPageListFiles.Controls.Add(this.metroGridFiles);
            this.metroTabPageListFiles.HorizontalScrollbarBarColor = true;
            this.metroTabPageListFiles.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPageListFiles.HorizontalScrollbarSize = 10;
            resources.ApplyResources(this.metroTabPageListFiles, "metroTabPageListFiles");
            this.metroTabPageListFiles.Name = "metroTabPageListFiles";
            this.metroTabPageListFiles.VerticalScrollbarBarColor = true;
            this.metroTabPageListFiles.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPageListFiles.VerticalScrollbarSize = 10;
            // 
            // metroGridFiles
            // 
            this.metroGridFiles.AllowDrop = true;
            this.metroGridFiles.AllowUserToResizeRows = false;
            this.metroGridFiles.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridFiles.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridFiles.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridFiles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGridFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridFiles.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGridFiles.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.metroGridFiles, "metroGridFiles");
            this.metroGridFiles.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridFiles.MultiSelect = false;
            this.metroGridFiles.Name = "metroGridFiles";
            this.metroGridFiles.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridFiles.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGridFiles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // metroComboBoxSort
            // 
            this.metroComboBoxSort.FormattingEnabled = true;
            resources.ApplyResources(this.metroComboBoxSort, "metroComboBoxSort");
            this.metroComboBoxSort.Items.AddRange(new object[] {
            resources.GetString("metroComboBoxSort.Items"),
            resources.GetString("metroComboBoxSort.Items1"),
            resources.GetString("metroComboBoxSort.Items2")});
            this.metroComboBoxSort.Name = "metroComboBoxSort";
            this.metroComboBoxSort.UseSelectable = true;
            this.metroComboBoxSort.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxSort_SelectedIndexChanged);
            // 
            // fileBindingSource
            // 
            this.fileBindingSource.DataSource = typeof(CryptoFileBrowser.Models.File);
            // 
            // Cabinet
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanelTypesFileView);
            this.Controls.Add(this.metroTabControl1);
            this.MaximizeBox = false;
            this.Name = "Cabinet";
            this.Resizable = false;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Cabinet_FormClosed);
            this.Load += new System.EventHandler(this.Cabinet_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPageProfile.ResumeLayout(false);
            this.metroTabPageProfile.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.metroPanelUserInfo.ResumeLayout(false);
            this.metroPanelUserInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.metroPanelInterface.ResumeLayout(false);
            this.metroPanelInterface.PerformLayout();
            this.metroTabPageAddFiles.ResumeLayout(false);
            this.metroTabPageAddFiles.PerformLayout();
            this.metroPanelFileTypes.ResumeLayout(false);
            this.metroPanelFileTypes.PerformLayout();
            this.metroTabPageBrowseFiles.ResumeLayout(false);
            this.metroTabPageBrowseFiles.PerformLayout();
            this.metroPanelTypesFileView.ResumeLayout(false);
            this.metroPanelTypesFileView.PerformLayout();
            this.metroTabPageExport.ResumeLayout(false);
            this.metroTabPageExport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager)).EndInit();
            this.metroTabPageListFiles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGridFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPageBrowseFiles;
        private MetroFramework.Controls.MetroTabPage metroTabPageAddFiles;
        private MetroFramework.Components.MetroStyleManager metroStyleManager;
        private MetroFramework.Controls.MetroTabPage metroTabPageProfile;
        private MetroFramework.Controls.MetroTabPage metroTabPageExport;
        private System.Windows.Forms.OpenFileDialog addFileDialog;
        private MetroFramework.Controls.MetroLabel metroLabelCipher;
        private MetroFramework.Controls.MetroToggle metroToggleCipher;
        private MetroFramework.Controls.MetroButton metroButtonBrowse;
        private MetroFramework.Controls.MetroLabel metroLabelAddFileName;
        private MetroFramework.Controls.MetroButton metroButtonAddFile;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelFiles;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanelUserInfo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroButton metroButtonChangeInfo;
        private MetroFramework.Controls.MetroLabel metroLabelName;
        private MetroFramework.Controls.MetroButton metroButtonChangePersonalKey;
        private MetroFramework.Controls.MetroLabel metroLabelSurname;
        private MetroFramework.Controls.MetroLabel metroLabelEmail;
        private MetroFramework.Controls.MetroPanel metroPanelInterface;
        private MetroFramework.Controls.MetroButton metroButtonSaveStyle;
        private MetroFramework.Controls.MetroComboBox metroComboBoxStyle;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroToggle metroToggleTheme;
        private MetroFramework.Controls.MetroPanel metroPanelFileTypes;
        private MetroFramework.Controls.MetroRadioButton metroRadioButtonTypeMedia;
        private MetroFramework.Controls.MetroRadioButton metroRadioButtonTypeGraphic;
        private MetroFramework.Controls.MetroRadioButton metroRadioButtonTypeText;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton metroButtonReset;
        private MetroFramework.Controls.MetroButton metroButtonExportInFolder;
        private MetroFramework.Controls.MetroButton metroButtonExportInEmail;
        private MetroFramework.Controls.MetroProgressSpinner metroProgressSpinnerExport;
        private MetroFramework.Controls.MetroProgressSpinner metroProgressSpinnerAddFile;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton1;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton2;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton3;
        private MetroFramework.Controls.MetroPanel metroPanelTypesFileView;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabelExportSize;
        private System.Windows.Forms.Panel metroPanelExportFiles;
        private System.Windows.Forms.SaveFileDialog folderBrowserDialogExport;
        private MetroFramework.Controls.MetroTabPage metroTabPageListFiles;
        private MetroFramework.Controls.MetroGrid metroGridFiles;
        private System.Windows.Forms.BindingSource fileBindingSource;
        private MetroFramework.Controls.MetroComboBox metroComboBoxSort;
    }
}