﻿namespace CryptoFileBrowser.Forms
{
    partial class Authorization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Authorization));
            this.metroTextBoxLogin = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonBrowse = new MetroFramework.Controls.MetroButton();
            this.metroLabelFile = new MetroFramework.Controls.MetroLabel();
            this.openPersonalKeyDialog = new System.Windows.Forms.OpenFileDialog();
            this.metroLinkRegistry = new MetroFramework.Controls.MetroLink();
            this.metroButtonEnter = new MetroFramework.Controls.MetroButton();
            this.metroLinkPasswordRestore = new MetroFramework.Controls.MetroLink();
            this.SuspendLayout();
            // 
            // metroTextBoxLogin
            // 
            // 
            // 
            // 
            this.metroTextBoxLogin.CustomButton.Image = null;
            this.metroTextBoxLogin.CustomButton.Location = new System.Drawing.Point(163, 1);
            this.metroTextBoxLogin.CustomButton.Name = "";
            this.metroTextBoxLogin.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxLogin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxLogin.CustomButton.TabIndex = 1;
            this.metroTextBoxLogin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxLogin.CustomButton.UseSelectable = true;
            this.metroTextBoxLogin.CustomButton.Visible = false;
            this.metroTextBoxLogin.Lines = new string[0];
            this.metroTextBoxLogin.Location = new System.Drawing.Point(57, 97);
            this.metroTextBoxLogin.MaxLength = 32767;
            this.metroTextBoxLogin.Name = "metroTextBoxLogin";
            this.metroTextBoxLogin.PasswordChar = '\0';
            this.metroTextBoxLogin.PromptText = "Логин";
            this.metroTextBoxLogin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxLogin.SelectedText = "";
            this.metroTextBoxLogin.SelectionLength = 0;
            this.metroTextBoxLogin.SelectionStart = 0;
            this.metroTextBoxLogin.ShortcutsEnabled = true;
            this.metroTextBoxLogin.Size = new System.Drawing.Size(185, 23);
            this.metroTextBoxLogin.TabIndex = 0;
            this.metroTextBoxLogin.UseSelectable = true;
            this.metroTextBoxLogin.WaterMark = "Логин";
            this.metroTextBoxLogin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxLogin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxPassword
            // 
            // 
            // 
            // 
            this.metroTextBoxPassword.CustomButton.Image = null;
            this.metroTextBoxPassword.CustomButton.Location = new System.Drawing.Point(163, 1);
            this.metroTextBoxPassword.CustomButton.Name = "";
            this.metroTextBoxPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxPassword.CustomButton.TabIndex = 1;
            this.metroTextBoxPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxPassword.CustomButton.UseSelectable = true;
            this.metroTextBoxPassword.CustomButton.Visible = false;
            this.metroTextBoxPassword.Lines = new string[0];
            this.metroTextBoxPassword.Location = new System.Drawing.Point(57, 137);
            this.metroTextBoxPassword.MaxLength = 32767;
            this.metroTextBoxPassword.Name = "metroTextBoxPassword";
            this.metroTextBoxPassword.PasswordChar = '*';
            this.metroTextBoxPassword.PromptText = "Пароль";
            this.metroTextBoxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxPassword.SelectedText = "";
            this.metroTextBoxPassword.SelectionLength = 0;
            this.metroTextBoxPassword.SelectionStart = 0;
            this.metroTextBoxPassword.ShortcutsEnabled = true;
            this.metroTextBoxPassword.Size = new System.Drawing.Size(185, 23);
            this.metroTextBoxPassword.TabIndex = 1;
            this.metroTextBoxPassword.UseSelectable = true;
            this.metroTextBoxPassword.WaterMark = "Пароль";
            this.metroTextBoxPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonBrowse
            // 
            this.metroButtonBrowse.Location = new System.Drawing.Point(38, 183);
            this.metroButtonBrowse.Name = "metroButtonBrowse";
            this.metroButtonBrowse.Size = new System.Drawing.Size(67, 23);
            this.metroButtonBrowse.TabIndex = 2;
            this.metroButtonBrowse.Text = "Обзор";
            this.metroButtonBrowse.UseSelectable = true;
            this.metroButtonBrowse.Click += new System.EventHandler(this.metroButtonBrowse_Click);
            // 
            // metroLabelFile
            // 
            this.metroLabelFile.AutoSize = true;
            this.metroLabelFile.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabelFile.Location = new System.Drawing.Point(111, 187);
            this.metroLabelFile.Name = "metroLabelFile";
            this.metroLabelFile.Size = new System.Drawing.Size(141, 15);
            this.metroLabelFile.TabIndex = 2;
            this.metroLabelFile.Text = "Выберите файл с ключом";
            // 
            // openPersonalKeyDialog
            // 
            this.openPersonalKeyDialog.DefaultExt = "dat";
            this.openPersonalKeyDialog.FileName = "personalKey";
            this.openPersonalKeyDialog.Title = "Открытие файла с персональным ключом";
            // 
            // metroLinkRegistry
            // 
            this.metroLinkRegistry.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroLinkRegistry.Location = new System.Drawing.Point(163, 235);
            this.metroLinkRegistry.Name = "metroLinkRegistry";
            this.metroLinkRegistry.Size = new System.Drawing.Size(114, 33);
            this.metroLinkRegistry.TabIndex = 4;
            this.metroLinkRegistry.Text = "Регистрация";
            this.metroLinkRegistry.UseSelectable = true;
            this.metroLinkRegistry.Click += new System.EventHandler(this.metroLinkRegistry_Click);
            // 
            // metroButtonEnter
            // 
            this.metroButtonEnter.Enabled = false;
            this.metroButtonEnter.Location = new System.Drawing.Point(23, 235);
            this.metroButtonEnter.Name = "metroButtonEnter";
            this.metroButtonEnter.Size = new System.Drawing.Size(119, 33);
            this.metroButtonEnter.TabIndex = 3;
            this.metroButtonEnter.Text = "Войти";
            this.metroButtonEnter.UseSelectable = true;
            this.metroButtonEnter.Click += new System.EventHandler(this.metroButtonEnter_Click);
            // 
            // metroLinkPasswordRestore
            // 
            this.metroLinkPasswordRestore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroLinkPasswordRestore.Location = new System.Drawing.Point(66, 288);
            this.metroLinkPasswordRestore.Name = "metroLinkPasswordRestore";
            this.metroLinkPasswordRestore.Size = new System.Drawing.Size(161, 23);
            this.metroLinkPasswordRestore.TabIndex = 5;
            this.metroLinkPasswordRestore.Text = "Восстановление пароля";
            this.metroLinkPasswordRestore.UseSelectable = true;
            this.metroLinkPasswordRestore.Click += new System.EventHandler(this.metroLinkPasswordRestore_Click);
            // 
            // Authorization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 334);
            this.Controls.Add(this.metroLinkPasswordRestore);
            this.Controls.Add(this.metroButtonEnter);
            this.Controls.Add(this.metroLinkRegistry);
            this.Controls.Add(this.metroLabelFile);
            this.Controls.Add(this.metroButtonBrowse);
            this.Controls.Add(this.metroTextBoxPassword);
            this.Controls.Add(this.metroTextBoxLogin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Authorization";
            this.Resizable = false;
            this.Text = "Авторизация";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox metroTextBoxLogin;
        private MetroFramework.Controls.MetroTextBox metroTextBoxPassword;
        private MetroFramework.Controls.MetroButton metroButtonBrowse;
        private MetroFramework.Controls.MetroLabel metroLabelFile;
        private System.Windows.Forms.OpenFileDialog openPersonalKeyDialog;
        private MetroFramework.Controls.MetroLink metroLinkRegistry;
        private MetroFramework.Controls.MetroButton metroButtonEnter;
        private MetroFramework.Controls.MetroLink metroLinkPasswordRestore;
    }
}