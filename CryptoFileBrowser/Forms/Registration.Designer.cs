﻿namespace CryptoFileBrowser.Forms
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registration));
            this.metroLabelName = new MetroFramework.Controls.MetroLabel();
            this.metroLabelSurname = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEmail = new MetroFramework.Controls.MetroLabel();
            this.metroLabelLogin = new MetroFramework.Controls.MetroLabel();
            this.metroLabelPassword = new MetroFramework.Controls.MetroLabel();
            this.metroLabelRestoreQuestion = new MetroFramework.Controls.MetroLabel();
            this.metroLabelAnswer = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxName = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxSurname = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxLogin = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxAnswer = new MetroFramework.Controls.MetroTextBox();
            this.richTextBox_RestoreQuestion = new System.Windows.Forms.RichTextBox();
            this.metroButtonRegistry = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.savePersonalKey = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // metroLabelName
            // 
            resources.ApplyResources(this.metroLabelName, "metroLabelName");
            this.metroLabelName.Name = "metroLabelName";
            // 
            // metroLabelSurname
            // 
            resources.ApplyResources(this.metroLabelSurname, "metroLabelSurname");
            this.metroLabelSurname.Name = "metroLabelSurname";
            // 
            // metroLabelEmail
            // 
            resources.ApplyResources(this.metroLabelEmail, "metroLabelEmail");
            this.metroLabelEmail.Name = "metroLabelEmail";
            // 
            // metroLabelLogin
            // 
            resources.ApplyResources(this.metroLabelLogin, "metroLabelLogin");
            this.metroLabelLogin.Name = "metroLabelLogin";
            // 
            // metroLabelPassword
            // 
            resources.ApplyResources(this.metroLabelPassword, "metroLabelPassword");
            this.metroLabelPassword.Name = "metroLabelPassword";
            // 
            // metroLabelRestoreQuestion
            // 
            resources.ApplyResources(this.metroLabelRestoreQuestion, "metroLabelRestoreQuestion");
            this.metroLabelRestoreQuestion.Name = "metroLabelRestoreQuestion";
            // 
            // metroLabelAnswer
            // 
            resources.ApplyResources(this.metroLabelAnswer, "metroLabelAnswer");
            this.metroLabelAnswer.Name = "metroLabelAnswer";
            // 
            // metroTextBoxName
            // 
            // 
            // 
            // 
            this.metroTextBoxName.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.metroTextBoxName.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode")));
            this.metroTextBoxName.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location")));
            this.metroTextBoxName.CustomButton.Name = "";
            this.metroTextBoxName.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size")));
            this.metroTextBoxName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxName.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex")));
            this.metroTextBoxName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxName.CustomButton.UseSelectable = true;
            this.metroTextBoxName.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible")));
            this.metroTextBoxName.Lines = new string[0];
            resources.ApplyResources(this.metroTextBoxName, "metroTextBoxName");
            this.metroTextBoxName.MaxLength = 32767;
            this.metroTextBoxName.Name = "metroTextBoxName";
            this.metroTextBoxName.PasswordChar = '\0';
            this.metroTextBoxName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxName.SelectedText = "";
            this.metroTextBoxName.SelectionLength = 0;
            this.metroTextBoxName.SelectionStart = 0;
            this.metroTextBoxName.ShortcutsEnabled = true;
            this.metroTextBoxName.UseSelectable = true;
            this.metroTextBoxName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.metroTextBoxName_KeyPress);
            // 
            // metroTextBoxSurname
            // 
            // 
            // 
            // 
            this.metroTextBoxSurname.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            this.metroTextBoxSurname.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode1")));
            this.metroTextBoxSurname.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location1")));
            this.metroTextBoxSurname.CustomButton.Name = "";
            this.metroTextBoxSurname.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size1")));
            this.metroTextBoxSurname.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxSurname.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex1")));
            this.metroTextBoxSurname.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxSurname.CustomButton.UseSelectable = true;
            this.metroTextBoxSurname.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible1")));
            this.metroTextBoxSurname.Lines = new string[0];
            resources.ApplyResources(this.metroTextBoxSurname, "metroTextBoxSurname");
            this.metroTextBoxSurname.MaxLength = 32767;
            this.metroTextBoxSurname.Name = "metroTextBoxSurname";
            this.metroTextBoxSurname.PasswordChar = '\0';
            this.metroTextBoxSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxSurname.SelectedText = "";
            this.metroTextBoxSurname.SelectionLength = 0;
            this.metroTextBoxSurname.SelectionStart = 0;
            this.metroTextBoxSurname.ShortcutsEnabled = true;
            this.metroTextBoxSurname.UseSelectable = true;
            this.metroTextBoxSurname.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxSurname.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.metroTextBoxName_KeyPress);
            // 
            // metroTextBoxEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxEmail.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            this.metroTextBoxEmail.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode2")));
            this.metroTextBoxEmail.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location2")));
            this.metroTextBoxEmail.CustomButton.Name = "";
            this.metroTextBoxEmail.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size2")));
            this.metroTextBoxEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEmail.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex2")));
            this.metroTextBoxEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxEmail.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible2")));
            this.metroTextBoxEmail.Lines = new string[0];
            resources.ApplyResources(this.metroTextBoxEmail, "metroTextBoxEmail");
            this.metroTextBoxEmail.MaxLength = 32767;
            this.metroTextBoxEmail.Name = "metroTextBoxEmail";
            this.metroTextBoxEmail.PasswordChar = '\0';
            this.metroTextBoxEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEmail.SelectedText = "";
            this.metroTextBoxEmail.SelectionLength = 0;
            this.metroTextBoxEmail.SelectionStart = 0;
            this.metroTextBoxEmail.ShortcutsEnabled = true;
            this.metroTextBoxEmail.UseSelectable = true;
            this.metroTextBoxEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxLogin
            // 
            // 
            // 
            // 
            this.metroTextBoxLogin.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            this.metroTextBoxLogin.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode3")));
            this.metroTextBoxLogin.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location3")));
            this.metroTextBoxLogin.CustomButton.Name = "";
            this.metroTextBoxLogin.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size3")));
            this.metroTextBoxLogin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxLogin.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex3")));
            this.metroTextBoxLogin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxLogin.CustomButton.UseSelectable = true;
            this.metroTextBoxLogin.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible3")));
            this.metroTextBoxLogin.Lines = new string[0];
            resources.ApplyResources(this.metroTextBoxLogin, "metroTextBoxLogin");
            this.metroTextBoxLogin.MaxLength = 32767;
            this.metroTextBoxLogin.Name = "metroTextBoxLogin";
            this.metroTextBoxLogin.PasswordChar = '\0';
            this.metroTextBoxLogin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxLogin.SelectedText = "";
            this.metroTextBoxLogin.SelectionLength = 0;
            this.metroTextBoxLogin.SelectionStart = 0;
            this.metroTextBoxLogin.ShortcutsEnabled = true;
            this.metroTextBoxLogin.UseSelectable = true;
            this.metroTextBoxLogin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxLogin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxPassword
            // 
            // 
            // 
            // 
            this.metroTextBoxPassword.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            this.metroTextBoxPassword.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode4")));
            this.metroTextBoxPassword.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location4")));
            this.metroTextBoxPassword.CustomButton.Name = "";
            this.metroTextBoxPassword.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size4")));
            this.metroTextBoxPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxPassword.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex4")));
            this.metroTextBoxPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxPassword.CustomButton.UseSelectable = true;
            this.metroTextBoxPassword.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible4")));
            this.metroTextBoxPassword.Lines = new string[0];
            resources.ApplyResources(this.metroTextBoxPassword, "metroTextBoxPassword");
            this.metroTextBoxPassword.MaxLength = 32767;
            this.metroTextBoxPassword.Name = "metroTextBoxPassword";
            this.metroTextBoxPassword.PasswordChar = '*';
            this.metroTextBoxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxPassword.SelectedText = "";
            this.metroTextBoxPassword.SelectionLength = 0;
            this.metroTextBoxPassword.SelectionStart = 0;
            this.metroTextBoxPassword.ShortcutsEnabled = true;
            this.metroTextBoxPassword.UseSelectable = true;
            this.metroTextBoxPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxAnswer
            // 
            // 
            // 
            // 
            this.metroTextBoxAnswer.CustomButton.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            this.metroTextBoxAnswer.CustomButton.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("resource.ImeMode5")));
            this.metroTextBoxAnswer.CustomButton.Location = ((System.Drawing.Point)(resources.GetObject("resource.Location5")));
            this.metroTextBoxAnswer.CustomButton.Name = "";
            this.metroTextBoxAnswer.CustomButton.Size = ((System.Drawing.Size)(resources.GetObject("resource.Size5")));
            this.metroTextBoxAnswer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAnswer.CustomButton.TabIndex = ((int)(resources.GetObject("resource.TabIndex5")));
            this.metroTextBoxAnswer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAnswer.CustomButton.UseSelectable = true;
            this.metroTextBoxAnswer.CustomButton.Visible = ((bool)(resources.GetObject("resource.Visible5")));
            this.metroTextBoxAnswer.Lines = new string[0];
            resources.ApplyResources(this.metroTextBoxAnswer, "metroTextBoxAnswer");
            this.metroTextBoxAnswer.MaxLength = 32767;
            this.metroTextBoxAnswer.Name = "metroTextBoxAnswer";
            this.metroTextBoxAnswer.PasswordChar = '\0';
            this.metroTextBoxAnswer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAnswer.SelectedText = "";
            this.metroTextBoxAnswer.SelectionLength = 0;
            this.metroTextBoxAnswer.SelectionStart = 0;
            this.metroTextBoxAnswer.ShortcutsEnabled = true;
            this.metroTextBoxAnswer.UseSelectable = true;
            this.metroTextBoxAnswer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAnswer.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // richTextBox_RestoreQuestion
            // 
            resources.ApplyResources(this.richTextBox_RestoreQuestion, "richTextBox_RestoreQuestion");
            this.richTextBox_RestoreQuestion.Name = "richTextBox_RestoreQuestion";
            // 
            // metroButtonRegistry
            // 
            resources.ApplyResources(this.metroButtonRegistry, "metroButtonRegistry");
            this.metroButtonRegistry.Name = "metroButtonRegistry";
            this.metroButtonRegistry.UseSelectable = true;
            this.metroButtonRegistry.Click += new System.EventHandler(this.button_Registry_Click);
            // 
            // metroButtonCancel
            // 
            resources.ApplyResources(this.metroButtonCancel, "metroButtonCancel");
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.UseSelectable = true;
            this.metroButtonCancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // savePersonalKey
            // 
            this.savePersonalKey.DefaultExt = "dat";
            this.savePersonalKey.FileName = "personalKey";
            resources.ApplyResources(this.savePersonalKey, "savePersonalKey");
            // 
            // Registration
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonRegistry);
            this.Controls.Add(this.metroTextBoxAnswer);
            this.Controls.Add(this.metroTextBoxPassword);
            this.Controls.Add(this.metroTextBoxLogin);
            this.Controls.Add(this.metroTextBoxEmail);
            this.Controls.Add(this.metroTextBoxSurname);
            this.Controls.Add(this.metroTextBoxName);
            this.Controls.Add(this.metroLabelRestoreQuestion);
            this.Controls.Add(this.metroLabelAnswer);
            this.Controls.Add(this.metroLabelPassword);
            this.Controls.Add(this.metroLabelLogin);
            this.Controls.Add(this.metroLabelEmail);
            this.Controls.Add(this.metroLabelSurname);
            this.Controls.Add(this.metroLabelName);
            this.Controls.Add(this.richTextBox_RestoreQuestion);
            this.MaximizeBox = false;
            this.Name = "Registration";
            this.Resizable = false;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Registration_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabelName;
        private MetroFramework.Controls.MetroLabel metroLabelSurname;
        private MetroFramework.Controls.MetroLabel metroLabelEmail;
        private MetroFramework.Controls.MetroLabel metroLabelLogin;
        private MetroFramework.Controls.MetroLabel metroLabelPassword;
        private MetroFramework.Controls.MetroLabel metroLabelRestoreQuestion;
        private MetroFramework.Controls.MetroLabel metroLabelAnswer;
        private MetroFramework.Controls.MetroTextBox metroTextBoxName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxSurname;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEmail;
        private MetroFramework.Controls.MetroTextBox metroTextBoxLogin;
        private MetroFramework.Controls.MetroTextBox metroTextBoxPassword;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAnswer;
        private System.Windows.Forms.RichTextBox richTextBox_RestoreQuestion;
        private MetroFramework.Controls.MetroButton metroButtonRegistry;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
        private System.Windows.Forms.SaveFileDialog savePersonalKey;
    }
}

