﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using CryptoFileBrowser.Models;
using CryptoFileBrowser.Crypto;

namespace CryptoFileBrowser.Forms
{
    public partial class ChangePersonalKey : MetroFramework.Forms.MetroForm
    {
        private User user;
        private DataBaseContext context = new DataBaseContext();

        public ChangePersonalKey(User u)
        {
            InitializeComponent();
            user = u;
            metroTextBoxLogin.Text = user.Login;
            metroTextBoxQuestion.Text = user.RestoreQuestion;
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void metroButtonChange_Click(object sender, EventArgs e)
        {
            if (metroTextBoxLogin.Text == "" || metroTextBoxPassword.Text == "" || metroTextBoxQuestion.Text == "" || metroTextBoxAnswer.Text == "")
            {
                MetroFramework.MetroMessageBox.Show(this, "Заполните все поля!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                MD5Hash hash = new MD5Hash();
                Cipher cipher = new HybridCipher();
                string oldPersonalKey = user.PersonalKey,
                    newPersonalKey=hash.GetHashCode(metroTextBoxLogin.Text + metroTextBoxPassword.Text + metroTextBoxAnswer.Text);
                SaveNewPersonalKey(newPersonalKey);

                foreach (var file in context.Files.Where(f => f.UserId == user.Id))
                {
                    if (file.IsCoded == 1)
                    {
                        byte[] encryptFileOld=System.IO.File.ReadAllBytes(user.Path+file.Name);
                        byte[] fileContent = cipher.Decrypt(encryptFileOld, UTF8Encoding.UTF8.GetBytes(oldPersonalKey));
                        byte[] encryptFileNew = cipher.Encrypt(fileContent, UTF8Encoding.UTF8.GetBytes(newPersonalKey));
                        System.IO.File.Delete(user.Path + file.Name);
                        using (var stream=System.IO.File.Open(user.Path+file.Name,FileMode.Create))
                        {
                            stream.Write(encryptFileNew, 0, encryptFileNew.Length);
                        }
                    }
                }
                SaveKeyToDb(metroTextBoxLogin.Text,metroTextBoxPassword.Text,metroTextBoxQuestion.Text,metroTextBoxAnswer.Text,newPersonalKey);
                MetroFramework.MetroMessageBox.Show(this, "Вы успешно сменили ключ и файлы были перешифрованы", "Изменение");
                this.Close();
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveKeyToDb(string login,string password, string question, string answer,string key)
        {
            user.Login = login;
            user.Password = password;
            user.RestoreQuestion = question;
            user.RestoreAnswer = answer;
            user.PersonalKey = key;
            context.Entry(user).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        private void SaveNewPersonalKey(string key)
        {
            while (savePersonalKey.ShowDialog() != DialogResult.OK)
                MetroFramework.MetroMessageBox.Show(this, "Необходимо указать файл для сохранения нового персонального ключа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            using (StreamWriter writer = new StreamWriter(savePersonalKey.FileName))
                writer.WriteLine(key);
        }
    }
}
