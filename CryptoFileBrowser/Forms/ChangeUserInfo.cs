﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CryptoFileBrowser.Models;

namespace CryptoFileBrowser.Forms
{
    public partial class ChangeUserInfo : MetroFramework.Forms.MetroForm
    {
        private User user;
        private DataBaseContext context;

        public ChangeUserInfo(User u)
        {
            InitializeComponent();
            user = u;
            metroTextBoxName.Text = u.Name;
            metroTextBoxSurname.Text = u.Surname;
            metroTextBoxEmail.Text = u.Email;
            context = new DataBaseContext();
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void metroButtonChange_Click(object sender, EventArgs e)
        {
            try
            {
                user.Name = metroTextBoxName.Text;
                user.Surname = metroTextBoxSurname.Text;
                user.Email = metroTextBoxEmail.Text;
                context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                MetroFramework.MetroMessageBox.Show(this, $"Информация была успешно изменена!", "Изменение информации!", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
