﻿namespace CryptoFileBrowser.Forms
{
    partial class ChangeUserInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeUserInfo));
            this.metroTextBoxSurname = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelSurname = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelName = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelEmail = new MetroFramework.Controls.MetroLabel();
            this.metroButtonChange = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroTextBoxSurname
            // 
            // 
            // 
            // 
            this.metroTextBoxSurname.CustomButton.Image = null;
            this.metroTextBoxSurname.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.metroTextBoxSurname.CustomButton.Name = "";
            this.metroTextBoxSurname.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxSurname.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxSurname.CustomButton.TabIndex = 1;
            this.metroTextBoxSurname.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxSurname.CustomButton.UseSelectable = true;
            this.metroTextBoxSurname.CustomButton.Visible = false;
            this.metroTextBoxSurname.Lines = new string[0];
            this.metroTextBoxSurname.Location = new System.Drawing.Point(122, 97);
            this.metroTextBoxSurname.MaxLength = 32767;
            this.metroTextBoxSurname.Name = "metroTextBoxSurname";
            this.metroTextBoxSurname.PasswordChar = '\0';
            this.metroTextBoxSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxSurname.SelectedText = "";
            this.metroTextBoxSurname.SelectionLength = 0;
            this.metroTextBoxSurname.SelectionStart = 0;
            this.metroTextBoxSurname.ShortcutsEnabled = true;
            this.metroTextBoxSurname.Size = new System.Drawing.Size(160, 23);
            this.metroTextBoxSurname.TabIndex = 0;
            this.metroTextBoxSurname.UseSelectable = true;
            this.metroTextBoxSurname.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxSurname.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelSurname
            // 
            this.metroLabelSurname.AutoSize = true;
            this.metroLabelSurname.Location = new System.Drawing.Point(37, 99);
            this.metroLabelSurname.Name = "metroLabelSurname";
            this.metroLabelSurname.Size = new System.Drawing.Size(68, 19);
            this.metroLabelSurname.TabIndex = 1;
            this.metroLabelSurname.Text = "Фамилия:";
            // 
            // metroTextBoxName
            // 
            // 
            // 
            // 
            this.metroTextBoxName.CustomButton.Image = null;
            this.metroTextBoxName.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.metroTextBoxName.CustomButton.Name = "";
            this.metroTextBoxName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxName.CustomButton.TabIndex = 1;
            this.metroTextBoxName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxName.CustomButton.UseSelectable = true;
            this.metroTextBoxName.CustomButton.Visible = false;
            this.metroTextBoxName.Lines = new string[0];
            this.metroTextBoxName.Location = new System.Drawing.Point(121, 137);
            this.metroTextBoxName.MaxLength = 32767;
            this.metroTextBoxName.Name = "metroTextBoxName";
            this.metroTextBoxName.PasswordChar = '\0';
            this.metroTextBoxName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxName.SelectedText = "";
            this.metroTextBoxName.SelectionLength = 0;
            this.metroTextBoxName.SelectionStart = 0;
            this.metroTextBoxName.ShortcutsEnabled = true;
            this.metroTextBoxName.Size = new System.Drawing.Size(160, 23);
            this.metroTextBoxName.TabIndex = 0;
            this.metroTextBoxName.UseSelectable = true;
            this.metroTextBoxName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelName
            // 
            this.metroLabelName.AutoSize = true;
            this.metroLabelName.Location = new System.Drawing.Point(67, 139);
            this.metroLabelName.Name = "metroLabelName";
            this.metroLabelName.Size = new System.Drawing.Size(38, 19);
            this.metroLabelName.TabIndex = 1;
            this.metroLabelName.Text = "Имя:";
            // 
            // metroTextBoxEmail
            // 
            // 
            // 
            // 
            this.metroTextBoxEmail.CustomButton.Image = null;
            this.metroTextBoxEmail.CustomButton.Location = new System.Drawing.Point(138, 1);
            this.metroTextBoxEmail.CustomButton.Name = "";
            this.metroTextBoxEmail.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxEmail.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEmail.CustomButton.TabIndex = 1;
            this.metroTextBoxEmail.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEmail.CustomButton.UseSelectable = true;
            this.metroTextBoxEmail.CustomButton.Visible = false;
            this.metroTextBoxEmail.Lines = new string[0];
            this.metroTextBoxEmail.Location = new System.Drawing.Point(121, 177);
            this.metroTextBoxEmail.MaxLength = 32767;
            this.metroTextBoxEmail.Name = "metroTextBoxEmail";
            this.metroTextBoxEmail.PasswordChar = '\0';
            this.metroTextBoxEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEmail.SelectedText = "";
            this.metroTextBoxEmail.SelectionLength = 0;
            this.metroTextBoxEmail.SelectionStart = 0;
            this.metroTextBoxEmail.ShortcutsEnabled = true;
            this.metroTextBoxEmail.Size = new System.Drawing.Size(160, 23);
            this.metroTextBoxEmail.TabIndex = 0;
            this.metroTextBoxEmail.UseSelectable = true;
            this.metroTextBoxEmail.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEmail.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelEmail
            // 
            this.metroLabelEmail.AutoSize = true;
            this.metroLabelEmail.Location = new System.Drawing.Point(55, 179);
            this.metroLabelEmail.Name = "metroLabelEmail";
            this.metroLabelEmail.Size = new System.Drawing.Size(50, 19);
            this.metroLabelEmail.TabIndex = 1;
            this.metroLabelEmail.Text = "E-mail:";
            // 
            // metroButtonChange
            // 
            this.metroButtonChange.Location = new System.Drawing.Point(23, 225);
            this.metroButtonChange.Name = "metroButtonChange";
            this.metroButtonChange.Size = new System.Drawing.Size(140, 40);
            this.metroButtonChange.TabIndex = 2;
            this.metroButtonChange.Text = "Изменить";
            this.metroButtonChange.UseSelectable = true;
            this.metroButtonChange.Click += new System.EventHandler(this.metroButtonChange_Click);
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(189, 225);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(136, 40);
            this.metroButtonCancel.TabIndex = 2;
            this.metroButtonCancel.Text = "Отмена";
            this.metroButtonCancel.UseSelectable = true;
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // ChangeUserInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 284);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonChange);
            this.Controls.Add(this.metroLabelEmail);
            this.Controls.Add(this.metroLabelName);
            this.Controls.Add(this.metroLabelSurname);
            this.Controls.Add(this.metroTextBoxEmail);
            this.Controls.Add(this.metroTextBoxName);
            this.Controls.Add(this.metroTextBoxSurname);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChangeUserInfo";
            this.Resizable = false;
            this.Text = "Изменение информации";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox metroTextBoxSurname;
        private MetroFramework.Controls.MetroLabel metroLabelSurname;
        private MetroFramework.Controls.MetroTextBox metroTextBoxName;
        private MetroFramework.Controls.MetroLabel metroLabelName;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEmail;
        private MetroFramework.Controls.MetroLabel metroLabelEmail;
        private MetroFramework.Controls.MetroButton metroButtonChange;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
    }
}