﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using CryptoFileBrowser.Models;
using System.Net.Mail;
using System.Net;

namespace CryptoFileBrowser.Forms
{
    public partial class PasswordRestore : MetroFramework.Forms.MetroForm
    {
        private string personalKey = String.Empty;
        private User user;

        public PasswordRestore()
        {
            InitializeComponent();
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void metroButtonBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialogPersonalKey.ShowDialog() == DialogResult.OK)
                {
                    using (Stream stream = openFileDialogPersonalKey.OpenFile())
                    {
                        byte[] buffer = new byte[stream.Length];
                        await stream.ReadAsync(buffer, 0, (int)stream.Length);
                        personalKey = UTF8Encoding.UTF8.GetString(buffer);
                        personalKey = personalKey.Remove(personalKey.Length - 2, 2);
                    }
                    metroLabelFile.Text = "Ключ был успешно считан!";
                    metroButtonOk.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroButtonFind_Click(object sender, EventArgs e)
        {
            if (metroTextBoxEmail.Text == "")
            {
                MetroFramework.MetroMessageBox.Show(this, "Вы не заполнили поле E-mail", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataBaseContext context = new DataBaseContext();
            List<User> users=context.Users.Where(u => u.Email == metroTextBoxEmail.Text).ToList();
            if (users.Count == 0)
                MetroFramework.MetroMessageBox.Show(this, "Вы не верное ввели E-mail", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                user = users.First();
                metroTextBoxRestoreQuestion.Text = user.RestoreQuestion;
            }
        }

        private void metroButtonOk_Click(object sender, EventArgs e)
        {
            if (metroTextBoxRestoreAnswer.Text == "")
            {
                MetroFramework.MetroMessageBox.Show(this, "Вы не заполнили поле ответа", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (user.RestoreAnswer == metroTextBoxRestoreAnswer.Text)
            {
                MailMessage message = new MailMessage("gorshkovserv@gmail.com", user.Email, "Восстановление пароля", $"Здравствуйте {user.Surname} {user.Name}<br/>.Ваш пароль: {user.Password}.");
                message.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("gorshkovserv@gmail.com", "gorshkovserv1998");
                smtp.Send(message);
                MetroFramework.MetroMessageBox.Show(this, "Пароль был выслан вам на E-mail", "Готово!");
                this.Close();
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Ответ дан неверный!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
