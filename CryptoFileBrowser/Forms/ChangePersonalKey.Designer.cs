﻿namespace CryptoFileBrowser.Forms
{
    partial class ChangePersonalKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePersonalKey));
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.metroButtonChange = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxAnswer = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxPassword = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxLogin = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelRestoreQuestion = new MetroFramework.Controls.MetroLabel();
            this.metroLabelAnswer = new MetroFramework.Controls.MetroLabel();
            this.metroLabelPassword = new MetroFramework.Controls.MetroLabel();
            this.metroLabelLogin = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxQuestion = new MetroFramework.Controls.MetroTextBox();
            this.savePersonalKey = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(242, 324);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(145, 43);
            this.metroButtonCancel.TabIndex = 28;
            this.metroButtonCancel.Text = "Отмена";
            this.metroButtonCancel.UseSelectable = true;
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // metroButtonChange
            // 
            this.metroButtonChange.Location = new System.Drawing.Point(75, 324);
            this.metroButtonChange.Name = "metroButtonChange";
            this.metroButtonChange.Size = new System.Drawing.Size(145, 43);
            this.metroButtonChange.TabIndex = 27;
            this.metroButtonChange.Text = "Изменить";
            this.metroButtonChange.UseSelectable = true;
            this.metroButtonChange.Click += new System.EventHandler(this.metroButtonChange_Click);
            // 
            // metroTextBoxAnswer
            // 
            // 
            // 
            // 
            this.metroTextBoxAnswer.CustomButton.Image = null;
            this.metroTextBoxAnswer.CustomButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.metroTextBoxAnswer.CustomButton.Location = new System.Drawing.Point(125, 1);
            this.metroTextBoxAnswer.CustomButton.Name = "";
            this.metroTextBoxAnswer.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxAnswer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxAnswer.CustomButton.TabIndex = 1;
            this.metroTextBoxAnswer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxAnswer.CustomButton.UseSelectable = true;
            this.metroTextBoxAnswer.CustomButton.Visible = false;
            this.metroTextBoxAnswer.Lines = new string[0];
            this.metroTextBoxAnswer.Location = new System.Drawing.Point(176, 282);
            this.metroTextBoxAnswer.MaxLength = 32767;
            this.metroTextBoxAnswer.Name = "metroTextBoxAnswer";
            this.metroTextBoxAnswer.PasswordChar = '\0';
            this.metroTextBoxAnswer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxAnswer.SelectedText = "";
            this.metroTextBoxAnswer.SelectionLength = 0;
            this.metroTextBoxAnswer.SelectionStart = 0;
            this.metroTextBoxAnswer.ShortcutsEnabled = true;
            this.metroTextBoxAnswer.Size = new System.Drawing.Size(147, 23);
            this.metroTextBoxAnswer.TabIndex = 22;
            this.metroTextBoxAnswer.UseSelectable = true;
            this.metroTextBoxAnswer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxAnswer.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxPassword
            // 
            // 
            // 
            // 
            this.metroTextBoxPassword.CustomButton.Image = null;
            this.metroTextBoxPassword.CustomButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.metroTextBoxPassword.CustomButton.Location = new System.Drawing.Point(147, 1);
            this.metroTextBoxPassword.CustomButton.Name = "";
            this.metroTextBoxPassword.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxPassword.CustomButton.TabIndex = 1;
            this.metroTextBoxPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxPassword.CustomButton.UseSelectable = true;
            this.metroTextBoxPassword.CustomButton.Visible = false;
            this.metroTextBoxPassword.Lines = new string[0];
            this.metroTextBoxPassword.Location = new System.Drawing.Point(176, 132);
            this.metroTextBoxPassword.MaxLength = 32767;
            this.metroTextBoxPassword.Name = "metroTextBoxPassword";
            this.metroTextBoxPassword.PasswordChar = '*';
            this.metroTextBoxPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxPassword.SelectedText = "";
            this.metroTextBoxPassword.SelectionLength = 0;
            this.metroTextBoxPassword.SelectionStart = 0;
            this.metroTextBoxPassword.ShortcutsEnabled = true;
            this.metroTextBoxPassword.Size = new System.Drawing.Size(169, 23);
            this.metroTextBoxPassword.TabIndex = 20;
            this.metroTextBoxPassword.UseSelectable = true;
            this.metroTextBoxPassword.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxPassword.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxLogin
            // 
            // 
            // 
            // 
            this.metroTextBoxLogin.CustomButton.Image = null;
            this.metroTextBoxLogin.CustomButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.metroTextBoxLogin.CustomButton.Location = new System.Drawing.Point(147, 1);
            this.metroTextBoxLogin.CustomButton.Name = "";
            this.metroTextBoxLogin.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxLogin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxLogin.CustomButton.TabIndex = 1;
            this.metroTextBoxLogin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxLogin.CustomButton.UseSelectable = true;
            this.metroTextBoxLogin.CustomButton.Visible = false;
            this.metroTextBoxLogin.Lines = new string[0];
            this.metroTextBoxLogin.Location = new System.Drawing.Point(176, 92);
            this.metroTextBoxLogin.MaxLength = 32767;
            this.metroTextBoxLogin.Name = "metroTextBoxLogin";
            this.metroTextBoxLogin.PasswordChar = '\0';
            this.metroTextBoxLogin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxLogin.SelectedText = "";
            this.metroTextBoxLogin.SelectionLength = 0;
            this.metroTextBoxLogin.SelectionStart = 0;
            this.metroTextBoxLogin.ShortcutsEnabled = true;
            this.metroTextBoxLogin.Size = new System.Drawing.Size(169, 23);
            this.metroTextBoxLogin.TabIndex = 19;
            this.metroTextBoxLogin.UseSelectable = true;
            this.metroTextBoxLogin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxLogin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelRestoreQuestion
            // 
            this.metroLabelRestoreQuestion.AutoSize = true;
            this.metroLabelRestoreQuestion.Location = new System.Drawing.Point(75, 172);
            this.metroLabelRestoreQuestion.Name = "metroLabelRestoreQuestion";
            this.metroLabelRestoreQuestion.Size = new System.Drawing.Size(186, 19);
            this.metroLabelRestoreQuestion.TabIndex = 23;
            this.metroLabelRestoreQuestion.Text = "Вопрос для восстановления:";
            // 
            // metroLabelAnswer
            // 
            this.metroLabelAnswer.AutoSize = true;
            this.metroLabelAnswer.Location = new System.Drawing.Point(102, 282);
            this.metroLabelAnswer.Name = "metroLabelAnswer";
            this.metroLabelAnswer.Size = new System.Drawing.Size(47, 19);
            this.metroLabelAnswer.TabIndex = 24;
            this.metroLabelAnswer.Text = "Ответ:";
            // 
            // metroLabelPassword
            // 
            this.metroLabelPassword.AutoSize = true;
            this.metroLabelPassword.Location = new System.Drawing.Point(102, 132);
            this.metroLabelPassword.Name = "metroLabelPassword";
            this.metroLabelPassword.Size = new System.Drawing.Size(57, 19);
            this.metroLabelPassword.TabIndex = 25;
            this.metroLabelPassword.Text = "Пароль:";
            // 
            // metroLabelLogin
            // 
            this.metroLabelLogin.AutoSize = true;
            this.metroLabelLogin.Location = new System.Drawing.Point(109, 92);
            this.metroLabelLogin.Name = "metroLabelLogin";
            this.metroLabelLogin.Size = new System.Drawing.Size(50, 19);
            this.metroLabelLogin.TabIndex = 26;
            this.metroLabelLogin.Text = "Логин:";
            // 
            // metroTextBoxQuestion
            // 
            // 
            // 
            // 
            this.metroTextBoxQuestion.CustomButton.Image = null;
            this.metroTextBoxQuestion.CustomButton.Location = new System.Drawing.Point(240, 2);
            this.metroTextBoxQuestion.CustomButton.Name = "";
            this.metroTextBoxQuestion.CustomButton.Size = new System.Drawing.Size(61, 61);
            this.metroTextBoxQuestion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxQuestion.CustomButton.TabIndex = 1;
            this.metroTextBoxQuestion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxQuestion.CustomButton.UseSelectable = true;
            this.metroTextBoxQuestion.CustomButton.Visible = false;
            this.metroTextBoxQuestion.Lines = new string[0];
            this.metroTextBoxQuestion.Location = new System.Drawing.Point(83, 199);
            this.metroTextBoxQuestion.MaxLength = 32767;
            this.metroTextBoxQuestion.Multiline = true;
            this.metroTextBoxQuestion.Name = "metroTextBoxQuestion";
            this.metroTextBoxQuestion.PasswordChar = '\0';
            this.metroTextBoxQuestion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxQuestion.SelectedText = "";
            this.metroTextBoxQuestion.SelectionLength = 0;
            this.metroTextBoxQuestion.SelectionStart = 0;
            this.metroTextBoxQuestion.ShortcutsEnabled = true;
            this.metroTextBoxQuestion.Size = new System.Drawing.Size(304, 66);
            this.metroTextBoxQuestion.TabIndex = 29;
            this.metroTextBoxQuestion.UseSelectable = true;
            this.metroTextBoxQuestion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxQuestion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // savePersonalKey
            // 
            this.savePersonalKey.Title = "Сохранение нового ключа";
            // 
            // ChangePersonalKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 394);
            this.Controls.Add(this.metroTextBoxQuestion);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonChange);
            this.Controls.Add(this.metroTextBoxAnswer);
            this.Controls.Add(this.metroTextBoxPassword);
            this.Controls.Add(this.metroTextBoxLogin);
            this.Controls.Add(this.metroLabelRestoreQuestion);
            this.Controls.Add(this.metroLabelAnswer);
            this.Controls.Add(this.metroLabelPassword);
            this.Controls.Add(this.metroLabelLogin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePersonalKey";
            this.Resizable = false;
            this.Text = "Изменение персонального ключа";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButtonCancel;
        private MetroFramework.Controls.MetroButton metroButtonChange;
        private MetroFramework.Controls.MetroTextBox metroTextBoxAnswer;
        private MetroFramework.Controls.MetroTextBox metroTextBoxPassword;
        private MetroFramework.Controls.MetroTextBox metroTextBoxLogin;
        private MetroFramework.Controls.MetroLabel metroLabelRestoreQuestion;
        private MetroFramework.Controls.MetroLabel metroLabelAnswer;
        private MetroFramework.Controls.MetroLabel metroLabelPassword;
        private MetroFramework.Controls.MetroLabel metroLabelLogin;
        private MetroFramework.Controls.MetroTextBox metroTextBoxQuestion;
        private System.Windows.Forms.SaveFileDialog savePersonalKey;
    }
}