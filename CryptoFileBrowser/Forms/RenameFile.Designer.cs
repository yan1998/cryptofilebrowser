﻿namespace CryptoFileBrowser.Forms
{
    partial class RenameFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenameFile));
            this.metroTextBoxNewFileName = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonSave = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.metroCheckBoxIsSave = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroTextBoxNewFileName
            // 
            // 
            // 
            // 
            this.metroTextBoxNewFileName.CustomButton.Image = null;
            this.metroTextBoxNewFileName.CustomButton.Location = new System.Drawing.Point(163, 1);
            this.metroTextBoxNewFileName.CustomButton.Name = "";
            this.metroTextBoxNewFileName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxNewFileName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxNewFileName.CustomButton.TabIndex = 1;
            this.metroTextBoxNewFileName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxNewFileName.CustomButton.UseSelectable = true;
            this.metroTextBoxNewFileName.CustomButton.Visible = false;
            this.metroTextBoxNewFileName.Lines = new string[0];
            this.metroTextBoxNewFileName.Location = new System.Drawing.Point(108, 73);
            this.metroTextBoxNewFileName.MaxLength = 32767;
            this.metroTextBoxNewFileName.Name = "metroTextBoxNewFileName";
            this.metroTextBoxNewFileName.PasswordChar = '\0';
            this.metroTextBoxNewFileName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxNewFileName.SelectedText = "";
            this.metroTextBoxNewFileName.SelectionLength = 0;
            this.metroTextBoxNewFileName.SelectionStart = 0;
            this.metroTextBoxNewFileName.ShortcutsEnabled = true;
            this.metroTextBoxNewFileName.Size = new System.Drawing.Size(185, 23);
            this.metroTextBoxNewFileName.TabIndex = 0;
            this.metroTextBoxNewFileName.UseSelectable = true;
            this.metroTextBoxNewFileName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxNewFileName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonSave
            // 
            this.metroButtonSave.Location = new System.Drawing.Point(23, 123);
            this.metroButtonSave.Name = "metroButtonSave";
            this.metroButtonSave.Size = new System.Drawing.Size(126, 36);
            this.metroButtonSave.TabIndex = 1;
            this.metroButtonSave.Text = "Сохранить";
            this.metroButtonSave.UseSelectable = true;
            this.metroButtonSave.Click += new System.EventHandler(this.metroButtonSave_Click);
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(188, 123);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(126, 36);
            this.metroButtonCancel.TabIndex = 2;
            this.metroButtonCancel.Text = "Отмена";
            this.metroButtonCancel.UseSelectable = true;
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // metroCheckBoxIsSave
            // 
            this.metroCheckBoxIsSave.AutoSize = true;
            this.metroCheckBoxIsSave.Location = new System.Drawing.Point(198, 52);
            this.metroCheckBoxIsSave.Name = "metroCheckBoxIsSave";
            this.metroCheckBoxIsSave.Size = new System.Drawing.Size(16, 0);
            this.metroCheckBoxIsSave.TabIndex = 3;
            this.metroCheckBoxIsSave.UseSelectable = true;
            this.metroCheckBoxIsSave.Visible = false;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(25, 74);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(79, 19);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Новое имя:";
            // 
            // RenameFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 180);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroCheckBoxIsSave);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonSave);
            this.Controls.Add(this.metroTextBoxNewFileName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RenameFile";
            this.Resizable = false;
            this.Text = "Переименование файла";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroButton metroButtonSave;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
        public MetroFramework.Controls.MetroCheckBox metroCheckBoxIsSave;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        public MetroFramework.Controls.MetroTextBox metroTextBoxNewFileName;
    }
}