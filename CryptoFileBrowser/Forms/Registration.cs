﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Text;
using System.IO;
using CryptoFileBrowser.Models;
using CryptoFileBrowser.Crypto;

namespace CryptoFileBrowser.Forms
{
    public partial class Registration : MetroFramework.Forms.MetroForm
    {
        private DataBaseContext context;
        private Authorization authorization;

        public Registration(Authorization authorization)
        {
            InitializeComponent();
            this.authorization = authorization;
            context = new DataBaseContext();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Registry_Click(object sender, EventArgs e)
        {
            try
            {
                CheckFillFiled();
                CheckExistsUser();
                string personalKey = GetPersonalKey();
                string path;
                AddUserToDB(personalKey,out path);
                SavePersonalKey(personalKey);
                Directory.CreateDirectory(path);
                Directory.CreateDirectory(path+"temp");
                MetroFramework.MetroMessageBox.Show(this, "Вы были успешно зарегестрированы!", "Регистрация");
                this.Close();
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Добавление пользователя в БД
        /// </summary>
        /// <param name="personalKey">строка- персональный ключ</param>
        /// <param name="path">Имя каталога, куда будут сохраняться файлы</param>
        private void AddUserToDB(string personalKey, out string path)
        {
            User user = new User()
            {
                Name = metroTextBoxName.Text,
                Surname = metroTextBoxSurname.Text,
                Email = metroTextBoxEmail.Text,
                Path = $"{metroTextBoxName.Text}-{metroTextBoxEmail.Text}/",
                PersonalKey = personalKey,
                Login = metroTextBoxLogin.Text,
                Password = metroTextBoxPassword.Text,
                RestoreQuestion = richTextBox_RestoreQuestion.Text,
                RestoreAnswer = metroTextBoxAnswer.Text,
            };
            context.Users.Add(user);
            context.SaveChanges();
            path = user.Path;
        }

        /// <summary>
        /// Вычисление персонального ключа пользователя при помощи алгоритма MD5.
        /// В качестве входной строки используются логин, пароль, и ключ для восстановления пароля.
        /// </summary>
        /// <returns>Строка - персональный ключ для дальнейшей шифрации файлов</returns>
        private string GetPersonalKey()
        {
            string str = metroTextBoxLogin.Text + metroTextBoxPassword.Text + metroTextBoxAnswer.Text;
            MD5Hash hash = new MD5Hash();
            return hash.GetHashCode(str);
        }

        /// <summary>
        /// Сохранение персонального ключа в файл
        /// </summary>
        /// <param name="personalKey">Строка- персональный ключ</param>
        private void SavePersonalKey(string personalKey)
        {
            while (savePersonalKey.ShowDialog() != DialogResult.OK)
                MetroFramework.MetroMessageBox.Show(this, "Необходимо указать файл для сохранения персонального ключа!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            using (StreamWriter writer = new StreamWriter(savePersonalKey.FileName))
                writer.WriteLine(personalKey);
        }

        /// <summary>
        /// Проверка на наличия пользователя с таким же логином в базе
        /// </summary>
        private void CheckExistsUser()
        {
            if (context.Users.Where(u => u.Login == metroTextBoxLogin.Text).Any())
                throw new Exception("Пользователь с таким логином существует!");
        }

        private void CheckFillFiled()
        {
            if (metroTextBoxLogin.Text=="" || metroTextBoxPassword.Text=="" || 
                metroTextBoxName.Text=="" || metroTextBoxSurname.Text=="" ||
                metroTextBoxEmail.Text=="" || metroTextBoxAnswer.Text=="" || richTextBox_RestoreQuestion.Text=="")
                throw new Exception("Заполните все поля!");
        }

        private void metroTextBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != (Char)Keys.Back)
                e.Handled = true;
        }

        private void Registration_FormClosed(object sender, FormClosedEventArgs e)
        {
            authorization.Visible = true;
        }
    }
}
