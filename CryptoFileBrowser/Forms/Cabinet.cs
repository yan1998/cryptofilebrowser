﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using MetroFramework;
using MetroFramework.Controls;
using MetroFramework.Components;
using System.Diagnostics;
using System.ComponentModel;
using CryptoFileBrowser.Models;
using CryptoFileBrowser.Crypto;
using System.Net.Mail;
using System.Net;
using Ionic.Zip;
using System.Collections.Generic;
using System.Data.Entity;

namespace CryptoFileBrowser.Forms
{
    public partial class Cabinet : MetroFramework.Forms.MetroForm
    {
        private DataBaseContext context;
        private User user;

        public Cabinet(User user)
        {
            InitializeComponent();
            this.user = user;
            this.StyleManager = metroStyleManager;
            if (user.Theme == 2)
                metroToggleTheme.Checked = true;
            metroComboBoxStyle.SelectedItem = metroComboBoxStyle.Items[user.Style];
            context = new DataBaseContext();
            metroLabelName.Text += user.Name;
            metroLabelSurname.Text += user.Surname;
            metroLabelEmail.Text += '\n' + user.Email;
            metroComboBoxSort.SelectedItem = metroComboBoxSort.Items[0];
            GetUserFiles();
        }

        private void Cabinet_Load(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(user.Path + "temp/"))
                System.IO.Directory.CreateDirectory(user.Path + "temp/");

           metroGridFiles.CellDoubleClick += MetroGridFiles_CellDoubleClick;
        }

        private void MetroGridFiles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            var row = metroGridFiles.Rows[e.RowIndex];
            Models.File file = new Models.File() {
                Name= row.Cells["Name"].Value.ToString(),
                IsCoded=(int)row.Cells["IsCoded"].Value
            };
            row.Tag=file;
            Tile_Click(row,null);
        }

        //Изменение темы персонального кабинета
        private void metroToggleTheme_CheckedChanged(object sender, EventArgs e)
        {
            metroStyleManager.Theme = metroStyleManager.Theme == MetroThemeStyle.Light ? MetroThemeStyle.Dark : MetroThemeStyle.Light;
        }

        //Выбор загружаемого файла
        private void metroButtonBrowse_Click(object sender, EventArgs e)
        {
            if (addFileDialog.ShowDialog() == DialogResult.OK)
                metroLabelAddFileName.Text = addFileDialog.SafeFileName;
        }

        //Добавление файла
        private void metroButtonAddFile_Click(object sender, EventArgs e)
        {
            metroProgressSpinnerAddFile.Visible = true;
            Thread thread = new Thread(() => {
                try
                {
                    if (metroLabelAddFileName.Text == "Выберите файл")
                        throw new Exception("Файл не был выбран!");
                    string fileName = addFileDialog.SafeFileName,
                        fileNameWithPath = addFileDialog.FileName;
                    long fileLength;
                    if (metroToggleCipher.Checked)
                    {
                        EncodeAndSaveFile(fileNameWithPath, ref fileName);
                        FileInfo info = new FileInfo(fileNameWithPath);
                        fileLength = info.Length;
                        AddFileToDataBase(fileName, fileLength, 1);
                    }
                    else
                    {
                        SaveFile(fileNameWithPath, ref fileName, out fileLength);
                        AddFileToDataBase(fileName, fileLength, 0);
                    }
                    MetroFramework.MetroMessageBox.Show(this, $"Файл {metroLabelAddFileName.Text} был успешно добавлен!", "Добавление!", MessageBoxButtons.OK, MessageBoxIcon.None);
                    addFileDialog.Reset();
                }
                catch (Exception ex)
                {
                    MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    AddFileSpinnerOff();
                }
            });
            thread.Start();
        }

        /// <summary>
        /// Добавление записи о файле в базу данных
        /// </summary>
        /// <param name="fileName">Имя загружаемого файла</param>
        /// <param name="size">Размер загружаемого файла до шифрования</param>
        /// <param name="isCoded">Зашифрован ли файл?</param>
        private async void AddFileToDataBase(string fileName, long size, int isCoded)
        {
            var checkedButton = metroPanelFileTypes.Controls.OfType<MetroRadioButton>()
                                      .FirstOrDefault(r => r.Checked);
            CryptoFileBrowser.Models.File file = new CryptoFileBrowser.Models.File()
            {
                Name = fileName,
                IsCoded = isCoded,
                UserId = user.Id,
                Size = size,
                Type = checkedButton.Text,
                Date = DateTime.Now
            };
            context.Files.Add(file);
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Сохранение файла без шифрования
        /// </summary>
        /// <param name="path">Путь и название файла, который будет загружен</param>
        /// <param name="fileName">Имя файла после загрузки</param>
        /// <param name="length">Размер файла</param>
        private void SaveFile(string path, ref string fileName, out long length)
        {
            FileInfo file = new FileInfo(path);
            length = file.Length;
            int i = 1;
            string newFileName = fileName;
            while (System.IO.File.Exists(user.Path + newFileName))
            {
                newFileName = $"({i.ToString()})-" + fileName;
                i++;
            }
            file.CopyTo(user.Path + newFileName, true);
            fileName = newFileName;
        }

        private void EncodeAndSaveFile(string path, ref string fileName)
        {
            byte[] file = System.IO.File.ReadAllBytes(path);
            Cipher cipher = new HybridCipher();
            byte[] encryptFile = cipher.Encrypt(file, UTF8Encoding.UTF8.GetBytes(user.PersonalKey));
            string newFileName = fileName;
            int i = 1;
            while (System.IO.File.Exists(user.Path + newFileName))
            {
                newFileName = $"({i.ToString()})-" + fileName;
                i++;
            }
            FileInfo saveFile = new FileInfo(user.Path + newFileName);
            using (FileStream stream = saveFile.Create())
            {
                stream.Write(encryptFile, 0, encryptFile.Length);
            }
            fileName = newFileName;
        }

        //Вывод всех файлов пользоватлеля
        private void GetUserFiles()
        {
            this.flowLayoutPanelFiles.Controls.Clear();

            var type = metroPanelTypesFileView.Controls.OfType<MetroRadioButton>()
                                      .FirstOrDefault(r => r.Checked).Tag.ToString();

            List<Models.File> files = new List<Models.File>();
            switch (metroComboBoxSort.SelectedItem)
            {
                case "По дате":
                    files = context.Files.Where(f => f.UserId == user.Id && f.Type == type).OrderBy(f => f.Date).ToList<Models.File>();
                    break;
                case "По размеру":
                    files = context.Files.Where(f => f.UserId == user.Id && f.Type == type).OrderBy(f => f.Size).ToList<Models.File>();
                    break;
                default:
                    files = context.Files.Where(f => f.UserId == user.Id && f.Type == type).OrderBy(f => f.Name).ToList<Models.File>();
                    break;
            }
            foreach (var file in files)
            {
                MetroTile tile = new MetroTile();
                ContextMenu contextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Открыть", Tile_Click){Tag=file },
                    new MenuItem("Переименовать",File_Rename_Click){ Tag=file},
                    new MenuItem("Удалить", File_Delete_Click){ Tag=file}
                });
                contextMenu.Tag = file.Name;
                tile.ContextMenu = contextMenu;
                tile.Name = "tileFile" + file.Id;
                tile.Text = file.Name;
                tile.Tag = file;
                tile.StyleManager = StyleManager;
                tile.Size = new Size(100, 100);
                MetroToolTip toolTip = new MetroToolTip();
                string isEncode = file.IsCoded == 0 ? "Нет" : "Да";
                toolTip.SetToolTip(tile, $"Название: {file.Name}\nРазмер: {Math.Round((file.Size / 1024)/1024.0,2)}Мб\nДата добавления: {file.Date.ToString("d")}\nЗашифрован? {isEncode}");
                tile.Click += Tile_Click;
                this.flowLayoutPanelFiles.Controls.Add(tile);
            }

            context.Files.Load();
            metroGridFiles.DataSource = context.Files.Local.Where(f => f.UserId == user.Id)
                .Select(x => new { x.Name, x.Type, x.Date, x.IsCoded })
                .OrderBy(f => f.Type)
                .ThenBy(f => f.Name)
                .ToList();
        }

        private void File_Delete_Click(object sender, EventArgs e)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    Menu m = (Menu)sender;
                    Models.File file = (Models.File)m.Tag;
                    context.Entry(file).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                    System.IO.File.Delete(user.Path + '\\' + file.Name);
                    transaction.Commit();
                    MetroFramework.MetroMessageBox.Show(this, $"Файл {file.Name} был успешно удалён!", "Добавление!", MessageBoxButtons.OK, MessageBoxIcon.None);
                    GetUserFiles();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void File_Rename_Click(object sender, EventArgs e)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    Menu m = (Menu)sender;
                    Models.File file = (Models.File)m.Tag;
                    RenameFile rename = new RenameFile(file.Name);
                    rename.ShowDialog();
                    if (rename.metroCheckBoxIsSave.Checked)
                    {
                        if (System.IO.File.Exists(user.Path + '\\' + rename.metroTextBoxNewFileName.Text))
                            throw new Exception("Файл с таким именем уже существует!");
                        System.IO.File.Move(user.Path + '\\' + file.Name, user.Path + '\\' + rename.metroTextBoxNewFileName.Text);
                        file.Name = rename.metroTextBoxNewFileName.Text;
                        context.Entry(file).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                        GetUserFiles();
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Tile_Click(object sender, EventArgs e)
        {
            try
            {
                dynamic control = sender;
                Models.File file = (Models.File)control.Tag;
                if (file.IsCoded == 0)
                    Process.Start(user.Path + '\\' + file.Name);
                else
                {
                    if (!System.IO.File.Exists(user.Path + "temp/" + file.Name))
                    {
                        Cipher cipher = new HybridCipher();
                        byte[] fileByteEncrypt = System.IO.File.ReadAllBytes(user.Path + file.Name);
                        byte[] fileByteDecrypt = cipher.Decrypt(fileByteEncrypt, UTF8Encoding.UTF8.GetBytes(user.PersonalKey));
                        FileInfo saveFile = new FileInfo(user.Path + "temp/" + file.Name);
                        using (FileStream stream = saveFile.Create())
                        {
                            stream.Write(fileByteDecrypt, 0, fileByteDecrypt.Length);
                        }
                    }
                    Process process = new Process();
                    process.EnableRaisingEvents = true;
                    process.Exited += DeleteTempFile;
                    process.StartInfo.FileName = user.Path + "temp\\" + file.Name;
                    bool b = process.Start();
                }
            }
            catch (Win32Exception)
            {
                MetroFramework.MetroMessageBox.Show(this, "Файл был удален!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void DeleteTempFile(object sender, EventArgs e)
        {
            Process process = (Process)sender;
            System.IO.File.Delete(process.StartInfo.FileName);
        }

        private void metroComboBoxStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroStyleManager.Style = (MetroColorStyle)metroComboBoxStyle.SelectedIndex;
        }

        private void metroButtonSaveStyle_Click(object sender, EventArgs e)
        {
            try
            {
                user.Theme = (int)metroStyleManager.Theme;
                user.Style = (int)metroStyleManager.Style;
                context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                MetroFramework.MetroMessageBox.Show(this, "Настройки интерфейса были успешно сохранены!", "Изменение!", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch (Exception)
            {
                MetroFramework.MetroMessageBox.Show(this, "Файл был удален!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroButtonReset_Click(object sender, EventArgs e)
        {
            if (user.Theme == 2)
                metroToggleTheme.Checked = true;
            else
                metroToggleTheme.Checked = false;
            metroComboBoxStyle.SelectedItem = metroComboBoxStyle.Items[user.Style];
        }

        private void metroButtonChangeInfo_Click(object sender, EventArgs e)
        {
            ChangeUserInfo change = new ChangeUserInfo(user);
            change.StyleManager = (MetroStyleManager)metroStyleManager.Clone();
            change.StyleManager.Owner = change;
            change.ShowDialog();
            metroLabelName.Text = "Имя: " + user.Name;
            metroLabelSurname.Text = "Фамилия: " + user.Surname;
            metroLabelEmail.Text = "E-mail: " + user.Email;
        }

        private void metroButtonChangePersonalKey_Click(object sender, EventArgs e)
        {
            ChangePersonalKey change = new ChangePersonalKey(user);
            change.StyleManager = (MetroStyleManager)metroStyleManager.Clone();
            change.StyleManager.Owner = change;
            change.ShowDialog();
        }

        private void metroButtonExportInFolder_Click(object sender, EventArgs e)
        {
            metroProgressSpinnerExport.Visible = true;
            Thread thread = new Thread(() =>
            {
                try
                {
                    using (ZipFile zip = new ZipFile(UTF8Encoding.UTF8))
                    {
                        zip.Password = user.Password;
                        foreach (var cb in metroPanelExportFiles.Controls.OfType<MetroCheckBox>().Where(r => r.Checked))
                        {
                            Models.File file = (Models.File)cb.Tag;
                            string path = user.Path;
                            if (file.IsCoded == 1)
                            {
                                path += "temp/";
                                Cipher cipher = new HybridCipher();
                                byte[] result = cipher.Decrypt(System.IO.File.ReadAllBytes(user.Path + file.Name), UTF8Encoding.UTF8.GetBytes(user.PersonalKey));
                                using (FileStream stream = System.IO.File.Create(path + file.Name))
                                    stream.Write(result, 0, result.Length);
                            }
                            zip.AddFile(path + file.Name, file.Type);
                        }
                        zip.Save(folderBrowserDialogExport.FileName);
                    }
                    MetroFramework.MetroMessageBox.Show(this, "Файлы были успешно экспортированы!", "Экспорт!", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
                catch (Exception)
                {
                    MetroFramework.MetroMessageBox.Show(this, "Файл был удален!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    ExportSpinnerOff();
                }
            });
            if (folderBrowserDialogExport.ShowDialog() == DialogResult.OK)
                thread.Start();
            else
                metroProgressSpinnerExport.Visible = false;
        }

        //Закрытие персонального кабинета
        private void Cabinet_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (System.IO.Directory.GetFiles(user.Path + "temp/").Length != 0)
            {
                System.IO.Directory.Delete(user.Path + "temp/", true);
                System.IO.Directory.CreateDirectory(user.Path + "temp/");
            }
            Application.Exit();
        }

        private void metroButtonExportInEmail_Click(object sender, EventArgs e)
        {
            if (float.Parse(metroLabelExportSize.Text) > 25)
            {
                MetroFramework.MetroMessageBox.Show(this,"Размер файлов превышает 25 Мб!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            metroProgressSpinnerExport.Visible = true;
            Thread thread = new Thread(()=> {
                MailMessage message = new MailMessage("gorshkovserv@gmail.com", user.Email, "Экспорт файлов", $"Здравствуйте {user.Surname} {user.Name}<br/>.Ваши файлы были успешно экспортированы! Пароль к архиву соответствует вашему паролю в программе!");
                message.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential("gorshkovserv@gmail.com", "gorshkovserv1998");
                try
                {
                    using (ZipFile zip = new ZipFile(UTF8Encoding.UTF8))
                    {
                        zip.Password = user.Password;
                        foreach (var cb in metroPanelExportFiles.Controls.OfType<MetroCheckBox>().Where(r => r.Checked))
                        {
                            Models.File file = (Models.File)cb.Tag;
                            string path = user.Path;
                            if (file.IsCoded == 1)
                            {
                                path += "temp/";
                                Cipher cipher = new HybridCipher();
                                byte[] result = cipher.Decrypt(System.IO.File.ReadAllBytes(user.Path + file.Name), UTF8Encoding.UTF8.GetBytes(user.PersonalKey));
                                using (FileStream stream = System.IO.File.Create(path + file.Name))
                                    stream.Write(result, 0, result.Length);
                            }
                            zip.AddFile(path + file.Name, file.Type);
                        }
                        zip.Save(user.Path + "temp/exportFiles.zip");
                    }
                    message.Attachments.Add(new Attachment(user.Path + "temp/exportFiles.zip"));
                    smtp.Send(message);
                    MetroFramework.MetroMessageBox.Show(this, $"Файл был успешно экспортирован!", "Экспорт файла!", MessageBoxButtons.OK, MessageBoxIcon.None);
                }
                catch (Exception ex)
                {
                    MetroFramework.MetroMessageBox.Show(this, ex.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    message.Dispose();
                    System.IO.Directory.Delete(user.Path + "temp/", true);
                    System.IO.Directory.CreateDirectory(user.Path + "temp/");
                    ExportSpinnerOff();
                }
            });
            thread.Start();
        }

        private void ExportSpinnerOff()
        {
            if (metroProgressSpinnerExport.InvokeRequired)
            {
                Action d = new Action(ExportSpinnerOff);
                this.Invoke(d);
            }
            else
                metroProgressSpinnerExport.Visible = false;
        }

        private void AddFileSpinnerOff()
        {
            if (metroProgressSpinnerExport.InvokeRequired)
            {
                Action d = new Action(AddFileSpinnerOff);
                this.Invoke(d);
            }
            else
            {
                metroLabelAddFileName.Text = "Выберите файл";
                GetUserFiles();
                metroProgressSpinnerAddFile.Visible = false;
            }
        }

        private void metroRadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            GetUserFiles();
        }

        private void metroTabPageExport_Click(object sender, EventArgs e)
        {
            metroPanelExportFiles.Controls.Clear();
            var groups = context.Files.Where(f=>f.UserId==user.Id)
                .OrderBy(f=>f.Name)
                .GroupBy(f=>f.Type, f=>f);
            int oy = 0;
            foreach (var group in groups)
            {
                MetroLabel label = new MetroLabel();
                label.Text = group.Key.Replace('й','е');
                label.StyleManager = StyleManager;
                label.Location = new Point(0,oy);
                oy += 20;
                metroPanelExportFiles.Controls.Add(label);
                foreach (var file in group)
                {
                    MetroCheckBox cb = new MetroCheckBox();
                    cb.Text = file.Name;
                    cb.Tag = file;
                    cb.Size = new Size(370,20);
                    cb.Location = new Point(20,oy);
                    cb.CheckedChanged += CheckedFileForExport;
                    cb.StyleManager = StyleManager;
                    metroPanelExportFiles.Controls.Add(cb);
                    oy += 20;
                }
            }
        }

        private void CheckedFileForExport(object sender, EventArgs e)
        {
            MetroCheckBox cb = (MetroCheckBox)sender;
            Models.File file = (Models.File)cb.Tag;
            float size = float.Parse(metroLabelExportSize.Text);
            if (cb.Checked)
                size += (float)Math.Round(file.Size / 1024 / 1024.0, 2);
            else
                size -= (float)Math.Round(file.Size / 1024 / 1024.0, 2);
            metroLabelExportSize.Text = size.ToString();
        }

        private void metroTabControl1_Selected(object sender, TabControlEventArgs e)
        {
            metroPanelTypesFileView.Visible = false;
            MetroTabControl tabControl = (MetroTabControl)sender;
            if (tabControl.SelectedIndex == 4)
                metroTabPageExport_Click(null, null);
            else
                if(tabControl.SelectedIndex==2)
                metroPanelTypesFileView.Visible = true;
        }

        private void metroComboBoxSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetUserFiles();
        }
    }
}
