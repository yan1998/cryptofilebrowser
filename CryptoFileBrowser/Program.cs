﻿#define DEV

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CryptoFileBrowser
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
#if (DEV)
            Models.DataBaseContext context = new Models.DataBaseContext();
            Models.User user = context.Users.Find(1);


            Application.Run(new CryptoFileBrowser.Forms.Cabinet(user));
#else
            Application.Run(new CryptoFileBrowser.Forms.Authorization());
#endif

        }
    }
}
