﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CryptoFileBrowser.Models
{
    public class User : INotifyPropertyChanged
    {
        private string name;
        private string surname;
        private string email;
        private string path;
        private string login;
        private string password;
        private string personalKey;
        private string restoreQuestion;
        private string restoreAnswer;
        private int style;
        private int theme;

        public int Id { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                OnPropertyChanged("Path");
            }
        }

        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                OnPropertyChanged("Login");
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                password= value;
                OnPropertyChanged("Password");
            }
        }

        public string PersonalKey
        {
            get { return personalKey; }
            set
            {
                personalKey = value;
                OnPropertyChanged("PersonalKey");
            }
        }

        public string RestoreQuestion
        {
            get { return restoreQuestion; }
            set
            {
                restoreQuestion = value;
                OnPropertyChanged("RestoreQuestion");
            }
        }

        public string RestoreAnswer
        {
            get { return restoreAnswer; }
            set
            {
                restoreAnswer = value;
                OnPropertyChanged("RestoreAnswer");
            }
        }

        public int Theme
        {
            get { return theme; }
            set
            {
                theme = value;
                OnPropertyChanged("Theme");
            }
        }

        public int Style
        {
            get { return style; }
            set
            {
                style = value;
                OnPropertyChanged("Style");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
