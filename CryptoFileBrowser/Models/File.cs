﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CryptoFileBrowser.Models
{
    public class File : INotifyPropertyChanged
    {
        private string name;
        private long size;
        private int isCoded;
        private DateTime date;
        private string type;

        public int Id { get; set; }

        public int UserId { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public long Size
        {
            get { return size; }
            set
            {
                size = value;
                OnPropertyChanged("Size");
            }
        }

        public int IsCoded
        {
            get { return isCoded; }
            set
            {
                isCoded = value;
                OnPropertyChanged("IsCoded");
            }
        }

        public DateTime Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }

        public string Type
        {
            get { return type; }
            set
            {
                type = value;
                OnPropertyChanged("Type");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
