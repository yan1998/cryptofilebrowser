﻿using System.Data.Entity;

namespace CryptoFileBrowser.Models
{
    class DataBaseContext:DbContext
    {
        public DataBaseContext():base("DefaultConnection"){}

        public DbSet<User> Users { get; set; }
        public DbSet<File> Files { get; set; }
    }
}
